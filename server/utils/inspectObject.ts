import util from 'util';
const inspectConfig = {
  depth: 3,
  maxArrayLength: 5,
  maxStringLength: 20,
  compact: true,
};

export function inspectObject(obj) {
  return util.inspect(obj, inspectConfig);
}
