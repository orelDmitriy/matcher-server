const nodeEnv = process.env.NODE_ENV;
export function isDevelopmentEnv(): boolean {
  return nodeEnv === 'development';
}

export function isTestingEnv(): boolean {
  return nodeEnv === 'testing';
}

export function isProductionEnv(): boolean {
  return !isDevelopmentEnv() && !isTestingEnv();
}

export function getRandomInt(min: number, max: number): number {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}
