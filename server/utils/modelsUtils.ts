import { HookNextFunction, model, Types } from 'mongoose';
import { objectUtils } from './objectUtils';
import { FieldsValidationError } from '../errors/FieldsValidationError';
import { ErrorsModels } from '../namespaces/ErrorsModels';
import { ErrorMessage } from '../constants/ErrorMessage';

export const modelsUtils = {
  validateAndPrepareFields: async function (
    next: HookNextFunction
  ): Promise<void> {
    await modelsUtils.checkReadonlyFields.call(this);
    await modelsUtils.checkForeignKeys.call(this);
    await modelsUtils.setUpdateTime.call(this);
    await next();
  },
  checkReadonlyFields: async function (): Promise<void> {
    if (this.isNew) {
      return;
    }

    const modifiedReadonlyFields: string[] = [];
    objectUtils.forEach(this.schema.paths, (fieldValue, fieldName) => {
      if (fieldValue.options?.readonly && this.isModified(fieldName)) {
        modifiedReadonlyFields.push(fieldName);
      }
    });

    if (modifiedReadonlyFields.length) {
      throw new FieldsValidationError(
        ErrorMessage.canNotModifierReadonly(),
        modelsUtils.createFieldsErrors(
          modifiedReadonlyFields,
          fieldName => ErrorMessage.canNotModifierReadonly(fieldName),
          FieldsValidationError.errorTypes.READONLY
        )
      );
    }
  },
  checkForeignKeys: async function (): Promise<void> {
    const foreignFields: string[] = [];
    const fields: string[] = Object.keys(this.schema.paths);
    for (let i = 0; i < fields.length; i++) {
      const fieldName: string = fields[i];
      const fieldValue = this.schema.paths[fieldName];
      const fieldOptions = fieldValue.options;

      const ref = fieldOptions.ref
        ? fieldOptions.ref
        : fieldOptions.type[0]?.ref;

      if (ref) {
        const ids = Array.isArray(this[fieldName])
          ? this[fieldName]
          : [this[fieldName]];
        const refModel = model(ref);
        for (let j = 0; j < ids.length; j++) {
          const currentId = ids[j];
          let refDocument = null;
          if (Types.ObjectId.isValid(currentId)) {
            refDocument = await refModel.findById(currentId);
          }

          if (!refDocument && fieldOptions.required) {
            foreignFields.push(fieldName);
            break;
          }
        }
      }
    }

    if (foreignFields.length) {
      throw new FieldsValidationError(
        ErrorMessage.canFoundForeignKey(),
        modelsUtils.createFieldsErrors(
          foreignFields,
          fieldName => ErrorMessage.canFoundForeignKey(fieldName),
          FieldsValidationError.errorTypes.FOREIGN_KEY
        )
      );
    }
  },

  createFieldsErrors: function (
    fieldsNames: string[],
    errorMessage: ((string) => string) | string,
    type: string
  ): ErrorsModels.FieldError[] {
    return fieldsNames.map<ErrorsModels.FieldError>(fieldName => {
      return {
        fieldName,
        message:
          typeof errorMessage === 'string'
            ? errorMessage
            : errorMessage(fieldName),
        type,
      };
    });
  },
  setUpdateTime: async function (): Promise<void> {
    if (this.isNew) {
      return;
    }

    this.updateAt = Date.now();
  },
};
