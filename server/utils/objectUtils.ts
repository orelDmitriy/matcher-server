type processedObject<T> = {
  [K in keyof T]?: any;
};

export const objectUtils = {
  forEach: (
    obj: object,
    func: (fieldValue: any, fieldName: string) => void
  ): void => {
    const keys = Object.keys(obj);
    keys.forEach(key => {
      func(obj[key], key);
    });
  },

  map: <T extends object>(
    obj: T,
    func: (fieldValue: any, fieldName: string) => any
  ): processedObject<T> => {
    const result: processedObject<T> = {};
    const keys = Object.keys(obj);
    keys.forEach(key => {
      result[key] = func(obj[key], key);
    });
    return result;
  },

  filter: <T extends object>(
    obj: T,
    func: (fieldValue: any, fieldName: string) => boolean
  ): processedObject<T> => {
    const result: processedObject<T> = {};
    const keys = Object.keys(obj);
    keys.forEach(key => {
      const value = obj[key];
      if (func(value, key)) {
        result[key] = value;
      }
    });

    return result;
  },

  reduce: <T>(
    obj: object,
    func: (result: T | Partial<T>, fieldValue: any, fieldName: string) => T,
    initial: T | Partial<T>
  ): T | Partial<T> => {
    const keys: string[] = Object.keys(obj);
    let currentResult = initial;
    keys.forEach(key => {
      const value = obj[key];
      currentResult = func(currentResult, value, key);
    });

    return currentResult;
  },
};
