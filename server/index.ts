import './configs/env';
import { ExpressServer } from './configs/ExpressServer';
import { WsServer } from './configs/webSocket/WsServer';
import { transmitterRoutes } from './configs/transmitterRoutes';
import { DatabaseConnection } from './configs/DatabaseConnection';
import { apiRoutes } from './configs/apiRoutes';

const database = new DatabaseConnection();
database.connect();

const webSocketServer = new WsServer();
webSocketServer.addWsRoutes(apiRoutes);

const transmitter = webSocketServer.getTransmitter();
transmitter.addRoutes(transmitterRoutes);
webSocketServer.listen();

const expressServer = new ExpressServer(transmitter);
expressServer.addRoutes(apiRoutes);

expressServer.listen();

export const application = expressServer.getApp();
