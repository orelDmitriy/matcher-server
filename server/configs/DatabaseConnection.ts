import mongoose from 'mongoose';
import logger from '../utils/logger';

const exit = process.exit;

export class DatabaseConnection {
  constructor() {
    mongoose.connection.on('disconnected', this.connect);
  }

  connect = (): void => {
    const dbUrl = this.getDbUrl();

    mongoose
      .connect(dbUrl, {
        useCreateIndex: true,
        useNewUrlParser: true,
        useUnifiedTopology: true,
      })
      .then(() => {
        return logger.info(`Successfully connected to ${dbUrl}`);
      })
      .catch(error => {
        logger.error('Error connecting to database: ', error);
        return exit(1);
      });
  };

  private getDbUrl = () => {
    const username: string = process.env.DB_USER;
    const password: string = process.env.DB_PASSWORD;
    const credentials = username && password ? `${username}:${password}@` : '';
    const host: string = process.env.DB_HOST;
    let port: string = process.env.DB_PORT;
    port = port ? `:${port}` : '';
    const dbName: string = process.env.DB_NAME;

    return `mongodb://${credentials}${host}${port}/${dbName}`;
  };
}
