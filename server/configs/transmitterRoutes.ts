import { ApiPath } from '../constants/ApiPath';
import { transmitterDialogController } from '../controllers/transmitter/transmitterDialog';
import { transmitterDialogMessageController } from '../controllers/transmitter/transmitterDialogMessage';
import { transmitterMessageController } from '../controllers/transmitter/transmitterMessage';
import { transmitterEventMessageController } from '../controllers/transmitter/transmitterEventMessage';
import { transmitterEventController } from '../controllers/transmitter/transmitterEvent';

export const transmitterRoutes = {
  [ApiPath.DIALOG]: transmitterDialogController,
  [ApiPath.DIALOG_MESSAGE]: transmitterDialogMessageController,
  [ApiPath.EVENT_MESSAGE]: transmitterEventMessageController,
  [ApiPath.MESSAGE]: transmitterMessageController,
  [ApiPath.EVENT]: transmitterEventController,
};
