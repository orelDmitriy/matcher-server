import { WsConnection } from './WsConnection';
import { WsModels } from '../../namespaces/WsModels';
import RequestData = WsModels.RequestData;
import ResponseData = WsModels.ResponseData;
import TransmitterRequestData = WsModels.TransmitterRequestData;
import TransmitterController = WsModels.TransmitterController;
import logger from '../../utils/logger';
import { WsServer } from './WsServer';

export class WsTransmitter {
  private server;
  private readonly routes = new Map<string, TransmitterController>();

  constructor(server: WsServer) {
    this.server = server;
  }

  async callSubscribers(
    requestData: RequestData,
    responseData: ResponseData
  ): Promise<void> {
    const controller = this.getController(requestData.path, requestData.method);
    if (!controller) {
      return;
    }

    try {
      await controller(requestData, responseData, this.jsonForUsers);
    } catch (error) {
      logger.error(error);
    }
  }

  jsonForUsers = (data: TransmitterRequestData, usersIds: string[]): void => {
    const connections: WsConnection[] = this.server.getUsersConnections(
      usersIds
    );
    connections.forEach(connection => {
      if (connection.isOpen()) {
        connection.send(data);
      }
    });
  };

  use(path: string, controller: TransmitterController): void {
    this.routes.set(path, controller);
  }

  private getController(path: string, method: string) {
    try {
      const fullPath = this.getFullPath(path);
      const controller = this.routes.get(fullPath);
      return controller?.[method.toLowerCase()];
    } catch (error) {
      logger.warn(error);
    }
  }

  private getFullPath(path): string {
    const paths = Array.from(this.routes.keys()).filter(controllerPath =>
      controllerPath.includes(path)
    );

    const fullPath = paths.find(controllerPath => controllerPath === path);
    if (fullPath) {
      return fullPath;
    }

    const withParams = paths
      .map(controllerPath => {
        const matchLastParams = controllerPath.match(/(\/[^\/]+\?)$/);
        if (!matchLastParams) {
          return;
        }

        return {
          input: controllerPath,
          withoutParam: controllerPath.slice(0, matchLastParams.index),
        };
      })
      .find(element => element.withoutParam === path);

    return withParams?.input;
  }

  addRoutes(transmitterRoutes: { [key: string]: TransmitterController }): void {
    for (const path in transmitterRoutes) {
      this.use(path, transmitterRoutes[path]);
    }
  }
}
