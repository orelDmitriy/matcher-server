import WebSocket, { Server } from 'ws';
import logger from '../../utils/logger';
import os from 'os';
import { WsConnection } from './WsConnection';
import { WsTransmitter } from './WsTransmitter';
import { WsModels } from '../../namespaces/WsModels';
import WsController = WsModels.WsController;
import WsControllerMethod = WsModels.WsControllerMethod;

export class WsServer {
  private server: Server;
  private readonly routes = new Map<string, Partial<WsController>>();
  private readonly transmitter: WsTransmitter;
  private readonly connections: WsConnection[] = [];

  constructor() {
    this.transmitter = new WsTransmitter(this);
  }

  listen(): void {
    const port = parseInt(process.env.WS_PORT);
    this.server = new Server({ port });
    this.server.on('connection', (ws: WebSocket) => {
      const connection = new WsConnection(ws, this);
      connection.listen();
      this.connections.push(connection);
    });

    logger.info(
      `WebSocket running in ${
        process.env.NODE_ENV || 'development'
      } @: ${os.hostname()} on port: ${port}}`
    );
  }

  private use(path: string, controller: Partial<WsModels.WsController>) {
    this.routes.set(path, controller);
  }

  addWsRoutes(wsRoutes: { [key: string]: WsController }): void {
    for (const path in wsRoutes) {
      this.use(path, wsRoutes[path]);
    }
  }

  getTransmitter(): WsTransmitter {
    return this.transmitter;
  }

  getController(path: string, method: string): WsControllerMethod {
    try {
      const controller = this.routes.get(path);
      return controller?.[method.toLowerCase()];
    } catch (error) {
      logger.warn(
        `Can't find controller by { path: ${path}, method: ${method} }. error: ${error}`
      );
    }
  }

  getUsersConnections(usersIds: string[]): WsConnection[] {
    return this.connections.filter(connection => {
      const connectionUserId = connection.getUserId();
      return connectionUserId && usersIds.includes(connectionUserId);
    });
  }
}
