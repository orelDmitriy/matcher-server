import WebSocket from 'ws';

import logger from '../../utils/logger';
import { ResponseCreator } from './ResponseCreator';
import { userService } from '../../servicies/userService';
import { WsModels } from '../../namespaces/WsModels';
import { ErrorMessage } from '../../constants/ErrorMessage';
import { UnauthorizedError } from '../../errors/UnauthorizedError';
import RequestData = WsModels.RequestData;
import RequestMethod = WsModels.RequestMethod;
import { WsServer } from './WsServer';
import { WsRequest } from './WsRequest';
import { ApiPath } from '../../constants/ApiPath';

export class WsConnection {
  private readonly ws: WebSocket;
  private readonly server: WsServer;

  private userId: string;
  private userToken: string;

  constructor(ws: WebSocket, server: WsServer) {
    this.ws = ws;
    this.server = server;
  }

  listen(): void {
    this.ws.on('message', async (requestMessage: string) => {
      const wsRequest = new WsRequest(this, requestMessage);

      if (this.isUserAuthorize()) {
        wsRequest.setAuth(this.userId, this.userToken);
      }

      const requestData = wsRequest.getRequestData();
      const responseCreator = await this.processRequest(requestData);
      const responseData = responseCreator.getFullResponse(
        requestData.actionId
      );

      await this.send(responseData);
      await this.server
        .getTransmitter()
        .callSubscribers(requestData, responseData);
    });
  }

  private async processRequest(
    requestData: RequestData
  ): Promise<ResponseCreator> {
    const responseCreator = new ResponseCreator();

    if (!requestData) {
      responseCreator.status(400);
      responseCreator.json({
        errorMessage: ErrorMessage.invalidRequest,
      });

      return responseCreator;
    }

    if (
      requestData.path === ApiPath.AUTHORIZATION &&
      requestData.method === RequestMethod.POST
    ) {
      await this.authorizeUser(requestData, responseCreator);

      return responseCreator;
    }

    try {
      const wsController = this.server.getController(
        requestData.path,
        requestData.method
      );

      if (!wsController) {
        responseCreator.status(400);
        responseCreator.json({
          errorMessage: ErrorMessage.invalidRequest,
        });

        return responseCreator;
      }

      await wsController(requestData, responseCreator);
    } catch (error) {
      logger.error(error);
      responseCreator.status(500);
      responseCreator.json({
        errorMessage: ErrorMessage.somethingWentWrong,
      });
    }

    return responseCreator;
  }

  private async authorizeUser(
    wsRequest: RequestData,
    responseCreator: ResponseCreator
  ) {
    try {
      const {
        body: { userId, token },
      } = wsRequest;

      await userService.checkCredential(userId, token);

      this.userId = userId;
      this.userToken = token;

      responseCreator.status(200);
      responseCreator.json({});
    } catch (error) {
      if (error.constructor === UnauthorizedError) {
        responseCreator.status(403);
        responseCreator.json({
          errorMessage: error.message,
        });

        return;
      }

      responseCreator.status(400);
      responseCreator.json({
        errorMessage: error.message || ErrorMessage.invalidRequest,
      });
    }
  }

  isUserAuthorize = (): boolean => {
    return Boolean(this.userId && this.userToken);
  };

  getUserId = (): string => {
    return this.userId;
  };

  isOpen(): boolean {
    return this.ws.readyState === 1;
  }

  send<T>(data: T): void {
    try {
      this.ws.send(JSON.stringify(data));
    } catch (error) {
      logger.warn(error);
    }
  }
}
