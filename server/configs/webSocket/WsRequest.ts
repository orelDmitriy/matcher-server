import logger from '../../utils/logger';
import { WsModels } from '../../namespaces/WsModels';
import RequestData = WsModels.RequestData;
import { WsConnection } from './WsConnection';

export class WsRequest {
  private readonly requestMessage;
  private readonly requestData: RequestData;

  constructor(connection: WsConnection, message: string) {
    this.requestMessage = message;
    this.requestData = this.messageToRequestData();
  }

  setAuth = (userId: string, userToken: string): void => {
    this.requestData.headers = {
      'auth-requester-id': userId,
      'auth-requester-token': userToken,
    };
  };

  getRequestData = (): RequestData => this.requestData;

  messageToRequestData(): RequestData {
    try {
      return JSON.parse(this.requestMessage);
    } catch (error) {
      logger.warn(error);
      return null;
    }
  }
}
