import { WsModels } from '../../namespaces/WsModels';
import ResponseBody = WsModels.ResponseBody;
import ResponseData = WsModels.ResponseData;

export class ResponseCreator {
  private _status = 200;
  private body: ResponseBody;

  status(status: number): void {
    this._status = status;
  }

  json(data: ResponseBody): void {
    this.body = data;
  }

  send(): void {
    this.body = undefined;
  }

  getFullResponse(actionId: string): ResponseData {
    return {
      actionId,
      status: this._status,
      body: this.body,
    };
  }
}
