import express, { Application } from 'express';
import path from 'path';
import bodyParser from 'body-parser';
import http from 'http';
import os from 'os';
import cookieParser from 'cookie-parser';
import morgan from 'morgan';

import logger from '../utils/logger';
import { requestLogger } from '../middlewares/requestLogger';
import { isProductionEnv } from '../utils/utilites';
import { WsTransmitter } from './webSocket/WsTransmitter';
import { wsTransmitter } from '../middlewares/wsTransmitter';
import { WsModels } from '../namespaces/WsModels';
import RequestMethod = WsModels.RequestMethod;

export class ExpressServer {
  private readonly app;
  private readonly transmitter;

  constructor(transmitter: WsTransmitter) {
    this.transmitter = transmitter;
    this.app = express();
    const root = path.normalize(__dirname + '/../..');

    this.app.set('appPath', root + 'client');
    this.app.use(morgan(isProductionEnv() ? 'combined' : 'dev'));
    this.app.use(
      bodyParser.json({ limit: process.env.REQUEST_LIMIT || '100kb' })
    );
    this.app.use(
      bodyParser.urlencoded({
        extended: true,
        limit: process.env.REQUEST_LIMIT || '100kb',
      })
    );
    this.app.use(
      bodyParser.text({ limit: process.env.REQUEST_LIMIT || '100kb' })
    );
    this.app.use(cookieParser(process.env.SESSION_SECRET));
    this.app.use(express.static(`${root}/public`));
    this.app.use(requestLogger);
    this.app.use(wsTransmitter(transmitter));
  }

  listen(): void {
    const port = parseInt(process.env.PORT);
    const welcome = (p: number) => (): void =>
      logger.info(
        `up and running in ${
          process.env.NODE_ENV || 'development'
        } @: ${os.hostname()} on port: ${p}}`
      );

    http.createServer(this.app).listen(port, welcome(port));
  }

  getApp = (): Application => {
    return this.app;
  };

  addRoutes(restApiRoutes: { [key: string]: any }): void {
    const entries = Object.entries(restApiRoutes);
    const router = express.Router();

    entries.forEach(([path, controller]) => {
      const methods = Object.keys(RequestMethod);
      methods.forEach(method => {
        const correctMethod = method.toLowerCase();
        if (controller[correctMethod]) {
          router[correctMethod](path, controller[correctMethod]);
        }
      });
    });

    this.app.use('/', router);
  }
}
