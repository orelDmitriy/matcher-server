import dotenv from 'dotenv';
import { isDevelopmentEnv, isTestingEnv } from '../utils/utilites';

dotenv.config({
  path: `./.env.${
    isDevelopmentEnv()
      ? 'development'
      : isTestingEnv()
      ? 'testing'
      : 'production'
  }`,
});
