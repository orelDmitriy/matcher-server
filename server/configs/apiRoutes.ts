import { userController } from '../controllers/user';
import { usersEventsController } from '../controllers/usersEvents';
import { usersDialogsController } from '../controllers/usersDialogs';
import { categoriesController } from '../controllers/categories';
import { categoryController } from '../controllers/category';
import { eventController } from '../controllers/event';
import { eventsController } from '../controllers/events';
import { dialogsController } from '../controllers/dialogs';
import { eventFollowerController } from '../controllers/eventFollower';
import { eventBlockedController } from '../controllers/eventBlocked';
import { eventMemberController } from '../controllers/eventMember';
import { ApiPath } from '../constants/ApiPath';
import { wsDialogController } from '../controllers/dialog';
import { wsMessageController } from '../controllers/message';
import { wsDialogMessageController } from '../controllers/dialogMessage';
import { wsEventMessageController } from '../controllers/eventMessage';

export const apiRoutes = {
  [ApiPath.USER]: userController,
  [ApiPath.USER_EVENTS]: usersEventsController,
  [ApiPath.USER_DIALOG]: usersDialogsController,
  [ApiPath.CATEGORY]: categoryController,
  [ApiPath.CATEGORIES]: categoriesController,
  [ApiPath.EVENT]: eventController,
  [ApiPath.EVENT_FOLLOWER]: eventFollowerController,
  [ApiPath.EVENT_BLOCKED]: eventBlockedController,
  [ApiPath.EVENT_MEMBER]: eventMemberController,
  [ApiPath.EVENTS]: eventsController,
  [ApiPath.DIALOGS]: dialogsController,
  [ApiPath.DIALOG]: wsDialogController,
  [ApiPath.MESSAGE]: wsMessageController,
  [ApiPath.DIALOG_MESSAGE]: wsDialogMessageController,
  [ApiPath.EVENT_MESSAGE]: wsEventMessageController,
};
