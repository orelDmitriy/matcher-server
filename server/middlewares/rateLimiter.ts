import { Request, Response, NextFunction } from 'express';
import { RateLimiterRedis } from 'rate-limiter-flexible';
import redis from 'redis';
import { ErrorMessage } from '../constants/ErrorMessage';

const redisClient = redis.createClient({
  enable_offline_queue: false,
});

const rateLimiterObject = new RateLimiterRedis({
  storeClient: redisClient,
  points: 5,
  duration: 5,
});

export const rateLimiter = async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> => {
  try {
    await rateLimiterObject.consume(req.connection.remoteAddress);
    next();
  } catch (error) {
    console.log(error);
    res.status(429).json({ errorMessage: ErrorMessage.tooManyRequests });
  }
};
