import { Request, Response, NextFunction } from 'express';
import logger from '../utils/logger';
import { isProductionEnv } from '../utils/utilites';
import { inspectObject } from '../utils/inspectObject';

export const requestLogger = async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> => {
  if (!isProductionEnv()) {
    const { url, params, body, method } = req;

    logger.info(
      `${method} ${url} params: ${inspectObject(params)} body: ${inspectObject(
        body
      )}`
    );
  }
  next();
};
