import { Request, Response, NextFunction } from 'express';
import { WsTransmitter } from '../configs/webSocket/WsTransmitter';
import { WsModels } from '../namespaces/WsModels';
import RequestData = WsModels.RequestData;
import ResponseData = WsModels.ResponseData;
import RequestMethod = WsModels.RequestMethod;

export const wsTransmitter = (transmitter: WsTransmitter) => async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> => {
  const send = res.send;

  const requestData: RequestData = {
    actionId: 'rest request call',
    path: req.originalUrl,
    method: RequestMethod[req.method],
    body: req.body,
  };

  res.send = function (body) {
    const responseData: ResponseData = {
      actionId: 'rest response call',
      status: this.statusCode,
      body,
    };

    transmitter.callSubscribers(requestData, responseData);
    return send.call(this, body);
  };

  const json = res.json;
  res.json = function (body) {
    const responseData: ResponseData = {
      actionId: 'rest response call',
      status: this.statusCode,
      body,
    };

    transmitter.callSubscribers(requestData, responseData);
    return json.call(this, body);
  };

  next();
};
