import { Fixture } from './Fixture';
import { MongoModels } from '../namespaces/MongoModels';
import ICategory = MongoModels.ICategory;
import { categoryService } from '../servicies/categoryService';

export class CategoryFixture extends Fixture<ICategory> {
  constructor(fixtureId: number) {
    super(fixtureId, {
      category: `some category. id: ${fixtureId}`,
    });
  }

  async save(overrideData: Partial<ICategory> = {}): Promise<void> {
    const categoryData = {
      ...this.defaultData,
      ...overrideData,
    };

    this.dataInDB = await categoryService.create(categoryData);
  }
}
