import { MongoModels } from '../namespaces/MongoModels';
import IEvent = MongoModels.IEvent;
import { eventService } from '../servicies/eventService';
import { UserFixture } from './UserFixture';
import { Fixture } from './Fixture';
import { CategoryFixture } from './CategoryFixture';
import { GlobalModels } from '../namespaces/GlobalModels';
import EventState = GlobalModels.EventState;
import { eventsFollowerService } from '../servicies/eventsFollowerService';
import FollowerState = GlobalModels.FollowerState;
import { eventMessageService } from '../servicies/eventMessageService';
import IMessage = MongoModels.IMessage;

export class EventFixture extends Fixture<IEvent> {
  constructor(fixtureId: number) {
    super(fixtureId, {
      date: Date.now(),
      useTime: false,
      description: `some event description with id: ${fixtureId}`,
      city: `Minsk ${fixtureId}`,
    });
  }

  async save(
    author: UserFixture,
    category: CategoryFixture,
    overrideData: Partial<IEvent> = {}
  ): Promise<void> {
    const eventData = {
      authorId: author.getField('_id'),
      categoryId: category.getField('_id'),
      ...this.defaultData,
      ...overrideData,
    };

    this.dataInDB = await eventService.create(eventData);
  }

  async close(): Promise<void> {
    await eventService.update(
      this.getId(),
      {
        state: EventState.closed,
      },
      this.getField('authorId')
    );
  }

  async addFollower(user: UserFixture): Promise<void> {
    await eventsFollowerService.addFollower(this.getId(), user.getId());
  }

  async addMember(user: UserFixture): Promise<void> {
    await this.addFollower(user);
    await eventsFollowerService.changeFollowerState(
      this.getId(),
      user.getId(),
      FollowerState.member
    );
  }

  async addBlocked(user: UserFixture): Promise<void> {
    await this.addFollower(user);
    await eventsFollowerService.changeFollowerState(
      this.getId(),
      user.getId(),
      FollowerState.blocked
    );
  }

  async addAllMessages(
    messages: Array<{
      creatorId: string;
      message: string;
    }>
  ): Promise<IEvent> {
    return eventMessageService.insertMany(this.getId(), messages);
  }

  async createMessage(user: UserFixture, message: string): Promise<IMessage> {
    return eventMessageService.create(this.getId(), user.getId(), message);
  }

  async getDataFromDB(): Promise<IEvent> {
    return eventService.getFullDataById(this.getId());
  }
}
