import { Fixture } from './Fixture';
import { MongoModels } from '../namespaces/MongoModels';
import { dialogService } from '../servicies/dialogService';
import { UserFixture } from './UserFixture';
import IDialog = MongoModels.IDialog;
import { dialogMessageService } from '../servicies/dialogMessageService';
import logger from '../utils/logger';
import IMessage = MongoModels.IMessage;

export class DialogFixture extends Fixture<IDialog> {
  constructor(fixtureId: number) {
    super(fixtureId);
  }

  async save(user1: UserFixture, user2: UserFixture): Promise<void> {
    this.dataInDB = await dialogService.create(
      user1.getField('_id'),
      user2.getField('_id')
    );
    logger.info(`Dialog with id ${this.fixtureId} saved.`);
  }

  async block(user: UserFixture) {
    await dialogService.block(this.getId(), user.getId());
  }

  async unblock(user: UserFixture) {
    await dialogService.unblock(this.getId(), user.getId());
  }

  async createMessage(user: UserFixture, message: string): Promise<IMessage> {
    return dialogMessageService.create(this.getId(), user.getId(), message);
  }

  async addAllMessages(
    messages: Array<{
      creatorId: string;
      message: string;
    }>
  ): Promise<IDialog> {
    return dialogMessageService.insertMany(this.getId(), messages);
  }
}
