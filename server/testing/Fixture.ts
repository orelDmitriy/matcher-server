export class Fixture<T extends { _id }> {
  protected fixtureId: number;
  protected defaultData: Partial<T>;
  protected dataInDB: T;

  constructor(fixtureId: number, defaultData: Partial<T> = {}) {
    this.fixtureId = fixtureId;
    this.defaultData = defaultData;
  }

  getField(fieldName: keyof T): any {
    return this.dataInDB?.[fieldName] || this.defaultData?.[fieldName];
  }

  getDefaultData(): Partial<T> {
    return this.defaultData;
  }

  getId(): string {
    // @ts-ignore
    return String(this.getField('_id'));
  }

  updateDefaultId(id: string): void {
    this.defaultData._id = id;
  }
}
