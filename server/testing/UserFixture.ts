import { MongoModels } from '../namespaces/MongoModels';
import { GlobalModels } from '../namespaces/GlobalModels';
import { userService } from '../servicies/userService';
import { Fixture } from './Fixture';
import IUser = MongoModels.IUser;
import Gender = GlobalModels.Gender;
import UserRole = GlobalModels.UserRole;
import UserState = GlobalModels.UserState;

export class UserFixture extends Fixture<IUser> {
  private readonly isAdmin;
  constructor(fixtureId: number, isAdmin = false) {
    super(fixtureId, {
      token: `token${fixtureId}`,
      nickname: `nickname${fixtureId}`,
      phone: 111111110 + fixtureId,
      gender: Gender.male,
      city: 'Minsk',
      country: 'by',
      photos: [],
    });
    this.isAdmin = isAdmin;
  }

  async save(overrideData: Partial<IUser> = {}): Promise<void> {
    const userData: Partial<IUser> = {
      ...this.defaultData,
      ...overrideData,
    };

    this.dataInDB = await userService.create(userData);

    if (this.isAdmin) {
      await userService.update(userData.nickname, {
        token: userData.token,
        role: UserRole.admin,
      });
    }
  }

  async block(): Promise<void> {
    await userService.update(this.getField('nickname'), {
      state: UserState.blocked,
    });
  }

  getAuthData(): {
    'auth-requester-token': string;
    'auth-requester-id': string;
  } {
    return {
      'auth-requester-token': this.getField('token'),
      'auth-requester-id': this.getId(),
    };
  }

  async getDataFromDB(): Promise<IUser> {
    return userService.getFullDataByNickname(this.getField('nickname'), true);
  }
}
