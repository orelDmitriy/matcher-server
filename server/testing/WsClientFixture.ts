import WebSocket from 'ws';
import { WsModels } from '../namespaces/WsModels';
import TransmitterRequestData = WsModels.TransmitterRequestData;
import ResponseData = WsModels.ResponseData;
import RequestData = WsModels.RequestData;
import { UserFixture } from './UserFixture';
import RequestMethod = WsModels.RequestMethod;
import { ApiPath } from '../constants/ApiPath';

export class WsClientFixture {
  private readonly ws: WebSocket;
  private answers: Array<ResponseData> = [];
  private requests: Array<TransmitterRequestData> = [];

  private promiseAnswer: Promise<void> = null;
  private waitCountAnswer = 0;
  private resolveAnswer: () => void = null;

  private promiseRequest: Promise<void> = null;
  private waitCountRequest = 0;
  private resolveRequest: () => void = null;

  constructor() {
    const port: number = parseInt(process.env.WS_PORT);
    this.ws = new WebSocket(`ws://localhost:${port}`);
    this.listen();
  }

  private listen(): void {
    this.ws.on('message', (message: string) => {
      const messageData = JSON.parse(message);
      console.log(`get: ${JSON.stringify(messageData)}`);
      if (messageData.actionId) {
        this.answers.push(messageData);
        this.answers.length <= this.waitCountAnswer && this.resolveAnswer?.();
      } else {
        this.requests.push(messageData);
        this.requests.length <= this.waitCountRequest &&
          this.resolveRequest?.();
      }
    });
  }

  async getRequests(): Promise<Array<TransmitterRequestData>> {
    await this.promiseRequest;
    return this.requests;
  }

  async getAnswers(): Promise<Array<ResponseData>> {
    await this.promiseAnswer;
    return this.answers;
  }

  send(data: RequestData): void {
    console.log(`send: ${JSON.stringify(data)}`);
    this.ws.send(JSON.stringify(data));
  }

  waitAnswers(count: number) {
    if (count > this.answers?.length) {
      this.waitCountAnswer = count;
      if (this.promiseAnswer) {
        this.resolveAnswer();
      }
      this.promiseAnswer = new Promise<void>(resolve => {
        this.resolveAnswer = () => {
          this.resolveAnswer = null;
          this.promiseAnswer = null;
          this.waitCountAnswer = 0;
          resolve();
        };
      });
    }
  }

  waitRequests(count: number, millisecond = 300) {
    if (count > this.requests.length) {
      this.waitCountRequest = count;

      if (this.promiseRequest) {
        this.resolveRequest();
      }

      this.promiseRequest = new Promise<void>(resolve => {
        let timer = null;
        this.resolveRequest = () => {
          this.resolveRequest = null;
          this.promiseRequest = null;
          this.waitCountRequest = 0;
          resolve();
          if (timer) {
            clearTimeout(timer);
            timer = null;
          }
        };

        timer = setTimeout(this.resolveRequest, millisecond);
      });
    }
  }

  clear() {
    this.answers = [];
    this.requests = [];
    this.resolveAnswer?.();
    this.resolveRequest?.();
  }

  async authorize(user: UserFixture) {
    this.send({
      actionId: 'correct',
      path: ApiPath.AUTHORIZATION,
      method: RequestMethod.POST,
      body: {
        userId: user.getId(),
        token: user.getField('token'),
      },
    });
    this.waitAnswers(1);
    await this.getAnswers();
    this.clear();
  }
}
