import { Request, Response } from 'express';
import { AccessError } from '../errors/AccessError';
import { ErrorMessage } from '../constants/ErrorMessage';

const requestHistory: { [key: string]: number } = {};

export function debounce(milliseconds: number) {
  return function (
    target: Object,
    name: string | symbol,
    descriptor: PropertyDescriptor
  ): PropertyDescriptor {
    const originalMethod = descriptor.value;
    if (typeof originalMethod === 'function') {
      descriptor.value = async function (req: Request, res: Response, ...rest) {
        const { baseUrl, method, headers } = req;
        const requestHash: string =
          method + baseUrl + headers['auth-requester-id'];
        const canCallOnTime = requestHistory[requestHash];
        const currentTime: number = Date.now();
        const nextCallTime: number = currentTime + milliseconds;
        if (canCallOnTime > currentTime) {
          throw new AccessError(
            ErrorMessage.actionBlockedUntil(
              new Date(canCallOnTime).toDateString()
            ),
            canCallOnTime
          );
        }

        const result = await originalMethod.call(this, req, res, ...rest);
        if (
          !res.statusCode ||
          (res.statusCode < 300 && res.statusCode >= 200)
        ) {
          requestHistory[requestHash] = nextCallTime;
        }

        return result;
      };
    }

    return descriptor;
  };
}
