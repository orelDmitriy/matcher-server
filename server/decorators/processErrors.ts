import { Request, Response } from 'express';
import { AccessError } from '../errors/AccessError';
import logger from '../utils/logger';
import { NotFoundError } from '../errors/NotFoundError';
import { FieldsValidationError } from '../errors/FieldsValidationError';
import { UnauthorizedError } from '../errors/UnauthorizedError';
import { ErrorsModels } from '../namespaces/ErrorsModels';
import { IncorrectRequestError } from '../errors/IncorrectRequestError';

export function processErrors() {
  return function (
    target: Object,
    name: string | symbol,
    descriptor: PropertyDescriptor
  ): any {
    const originalMethod = descriptor.value;
    if (typeof originalMethod === 'function') {
      descriptor.value = async function (req: Request, res: Response, ...rest) {
        try {
          return await originalMethod.call(this, req, res, ...rest);
        } catch (error) {
          const responseErrorData: ErrorsModels.ResponseErrorData = {
            errorMessage: error.message,
          };
          let errorLevel = false;
          switch (error.constructor) {
            case AccessError: {
              logger.warn(req.headers);
              res.status(403);
              responseErrorData.until = error.until;
              break;
            }
            case UnauthorizedError: {
              res.status(401);
              break;
            }
            case NotFoundError: {
              res.status(404);
              break;
            }
            case IncorrectRequestError:
            case FieldsValidationError: {
              res.status(400);
              responseErrorData.errors = error.fieldsErrors;
              break;
            }
            default: {
              errorLevel = true;
              res.status(500);
            }
          }

          errorLevel ? logger.error(error) : logger.info(error.message);
          res.json(responseErrorData);
        }
      };
    }

    return descriptor;
  };
}
