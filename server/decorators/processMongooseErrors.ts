import { objectUtils } from '../utils/objectUtils';
import { FieldsValidationError } from '../errors/FieldsValidationError';
import { ErrorsModels } from '../namespaces/ErrorsModels';
import logger from '../utils/logger';
import FieldError = ErrorsModels.FieldError;

export function processMongooseErrors() {
  return function (
    target: Object,
    name: string | symbol,
    descriptor: PropertyDescriptor
  ): PropertyDescriptor {
    const originalMethod = descriptor.value;
    if (typeof originalMethod === 'function') {
      descriptor.value = async function (...rest) {
        try {
          return await originalMethod.call(this, ...rest);
        } catch (error) {
          const { message, errors: fieldsErrors } = error;
          if (fieldsErrors) {
            const preparedErrors = objectUtils.reduce<FieldError[]>(
              fieldsErrors,
              (result, errorData, fieldName) => {
                result.push({
                  fieldName,
                  message: errorData?.properties?.message,
                  type: errorData?.properties?.type,
                });
                return result;
              },
              []
            );

            logger.info(error.message);
            throw new FieldsValidationError(message, preparedErrors);
          }

          throw error;
        }
      };
    }

    return descriptor;
  };
}
