import { Request } from 'express';
import { objectUtils } from '../utils/objectUtils';

export function allowedFields(allowedFields: string[]) {
  return function (
    target: Object,
    name: string | symbol,
    descriptor: PropertyDescriptor
  ): PropertyDescriptor {
    const originalMethod = descriptor.value;
    if (typeof originalMethod === 'function') {
      const allAllowedFields: string[] = [...allowedFields];
      descriptor.value = async function (req: Request, ...rest) {
        req.body = objectUtils.filter(req.body, (value, fieldName) =>
          allAllowedFields.includes(fieldName)
        );
        return await originalMethod.call(this, req, ...rest);
      };
    }

    return descriptor;
  };
}
