import logger from '../utils/logger';
import { NotFoundError } from '../errors/NotFoundError';

export function convertToNotFoundError() {
  return function (
    target: Object,
    name: string | symbol,
    descriptor: PropertyDescriptor
  ): PropertyDescriptor {
    const originalMethod = descriptor.value;
    if (typeof originalMethod === 'function') {
      descriptor.value = async function (...params) {
        try {
          return await originalMethod.call(this, ...params);
        } catch (error) {
          if (error.constructor === NotFoundError) {
            throw error;
          }

          logger.warn(error);
          throw new NotFoundError(
            `Can't found ${target.constructor.name} with params: ${String(
              params
            )}`
          );
        }
      };
    }

    return descriptor;
  };
}
