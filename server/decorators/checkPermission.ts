import { Request, Response } from 'express';
import { userService } from '../servicies/userService';

export function checkPermission(roles?: string[], continueRequest?: boolean) {
  return function (
    target: Object,
    name: string | symbol,
    descriptor: PropertyDescriptor
  ): PropertyDescriptor {
    const originalMethod = descriptor.value;
    if (typeof originalMethod === 'function') {
      descriptor.value = async function (req: Request, res: Response, ...rest) {
        const requesterId: string =
          typeof req.headers?.['auth-requester-id'] === 'string'
            ? req.headers['auth-requester-id']
            : null;
        const requesterToken: string =
          typeof req.headers?.['auth-requester-token'] === 'string'
            ? req.headers['auth-requester-token']
            : null;

        let user;
        try {
          user = await userService.checkCredential(
            requesterId,
            requesterToken,
            roles
          );
        } catch (error) {
          if (!continueRequest) {
            throw error;
          }
        }

        req.body = {
          ...(req.body || {}),
          checkedPermission: {
            authorized: !!user,
            nickname: user?.nickname,
            requesterId,
            token: requesterToken,
            role: user?.role,
          },
        };

        return await originalMethod.call(this, req, res, ...rest);
      };
    }

    return descriptor;
  };
}
