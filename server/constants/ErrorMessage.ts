export const ErrorMessage = {
  invalidRequest: 'Invalid request',
  somethingWentWrong: 'Something went wrong.',
  tooManyRequests: 'Too Many Requests',
  userAreNotFollower: 'User are not follower',
  dialogAlreadyBlocked: 'Dialog is already blocked',
  userDoesntExist: (nickname: string): string =>
    `User with nickname '${nickname}' doesnt exist`,
  userWithIdDoesntExist: (userId: string): string =>
    `Can't found user: ${userId}`,
  userAreNotMember: 'User are not member',
  userAreNotMemberOrFollower: 'User are not member or follower',
  userAreNotBlocked: 'User are not blocked',
  userHasState: (nickname: string, state: string): string =>
    `User ${nickname} is ${state}`,
  userMustHaveRole: (roles: string): string =>
    `User must have one role from ${roles}`,
  permissionDenied: 'Permission denied',
  actionBlockedUntil: (date: string): string => `Action blocked until ${date}`,
  accessClosed: 'Access closed',
  eventHasNotFollower: (eventId: string, followerId: string): string =>
    `Event ${eventId} hasn't follower ${followerId}`,
  tokenIsNotRight: 'Token is not right',
  canNotModifierReadonly: (fieldName?: string): string =>
    `Can't modifier readonly field ${fieldName || ''}`,
  canFoundForeignKey: (fieldName?: string): string =>
    `Can't found foreign key ${fieldName || ''}`,
  userBlocked: userId => `User ${userId} blocked`,
  userIsNotDialogParticipant: userId =>
    `User ${userId} is not dialog participant`,
  dialogAlreadyExist: `Dialog already exist`,
};
