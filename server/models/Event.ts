import { Schema, model } from 'mongoose';

import { MongoModels } from '../namespaces/MongoModels';
import IEvent = MongoModels.IEvent;
import { modelsUtils } from '../utils/modelsUtils';
import { GlobalModels } from '../namespaces/GlobalModels';
import EventState = GlobalModels.EventState;
import EventFor = GlobalModels.EventFor;

const EventSchema = new Schema({
  authorId: {
    type: String,
    ref: 'user',
    required: true,
    readonly: true,
  },
  messages: [
    {
      type: String,
      ref: 'message',
    },
  ],

  categoryId: {
    type: String,
    ref: 'category',
    required: true,
    readonly: true,
  },
  date: {
    type: Number,
    required: true,
    readonly: true,
  },
  useTime: {
    type: Boolean,
    readonly: true,
  },
  description: {
    type: String,
    required: true,
    minlength: 3,
    maxlength: 500,
    readonly: true,
  },
  photos: {
    type: [String],
    readonly: true,
  },
  city: {
    type: String,
    required: true,
    readonly: true,
    minlength: 3,
    maxlength: 30,
  },
  maxPerson: {
    type: Number,
    min: 1,
    max: 100,
  },
  for: {
    type: String,
    enum: Object.values(EventFor),
    default: EventFor.any,
    readonly: true,
  },
  anon: {
    type: Boolean,
    default: false,
    readonly: true,
  },
  state: {
    type: String,
    enum: Object.values(EventState),
    default: EventState.opened,
  },

  createAt: { type: Number, default: Date.now, readonly: true },
  updateAt: { type: Number, default: Date.now },
});

EventSchema.pre('save', modelsUtils.validateAndPrepareFields);

export const Event = model<IEvent>('event', EventSchema);
