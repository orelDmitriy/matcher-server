import { Schema, model } from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';

import { MongoModels } from '../namespaces/MongoModels';
import IUser = MongoModels.IUser;
import { modelsUtils } from '../utils/modelsUtils';
import { GlobalModels } from '../namespaces/GlobalModels';
import Gender = GlobalModels.Gender;
import UserState = GlobalModels.UserState;
import UserRole = GlobalModels.UserRole;

const UserSchema: Schema = new Schema({
  token: {
    type: String,
    required: true,
    unique: true,
  },
  role: {
    type: String,
    enum: Object.values(UserRole),
    default: UserRole.user,
  },

  phone: {
    type: Number,
    required: true,
    unique: true,
    readonly: true,
  },
  state: {
    type: String,
    enum: Object.values(UserState),
    default: UserState.active,
  },
  nickname: {
    type: String,
    required: true,
    minlength: 3,
    maxlength: 30,
    unique: true,
    readonly: true,
  },
  gender: {
    type: String,
    enum: Object.values(Gender),
    required: true,
  },
  city: {
    type: String,
    required: true,
  },
  country: {
    type: String,
    required: true,
  },

  photos: [String],
  vk: String,
  instagram: String,

  createAt: { type: Number, default: Date.now, readonly: true },
  updateAt: { type: Number, default: Date.now },
  lastActivity: { type: Number, default: Date.now },
});

UserSchema.plugin(uniqueValidator);
UserSchema.pre('save', modelsUtils.validateAndPrepareFields);

export const User = model<IUser>('user', UserSchema);
