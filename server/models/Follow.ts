import { Schema, model } from 'mongoose';

import { MongoModels } from '../namespaces/MongoModels';
import IFollow = MongoModels.IFollow;
import { modelsUtils } from '../utils/modelsUtils';

const FollowSchema = new Schema({
  userId: {
    type: String,
    ref: 'user',
    required: true,
  },
  message: {
    type: String,
    maxLength: 100,
  },
  showed: {
    type: Boolean,
    default: false,
  },

  createAt: { type: Number, default: Date.now, readonly: true },
});

FollowSchema.pre('save', modelsUtils.validateAndPrepareFields);

export const Follow = model<IFollow>('follow', FollowSchema);
