import { Schema, model } from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';

import { MongoModels } from '../namespaces/MongoModels';
import { modelsUtils } from '../utils/modelsUtils';
import IUsersEvent = MongoModels.IUsersEvent;

const UsersEventSchema: Schema = new Schema({
  userId: {
    type: String,
    ref: 'user',
    required: true,
    readonly: true,
  },

  eventId: {
    type: String,
    ref: 'event',
    required: true,
    readonly: true,
  },

  createAt: { type: Number, default: Date.now, readonly: true },
  updateAt: { type: Number, default: Date.now },
});

UsersEventSchema.plugin(uniqueValidator);
UsersEventSchema.pre('save', modelsUtils.validateAndPrepareFields);

export const UsersEvent = model<IUsersEvent>('users_event', UsersEventSchema);
