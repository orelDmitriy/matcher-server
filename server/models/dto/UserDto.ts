import { MongoModels } from '../../namespaces/MongoModels';
import IUser = MongoModels.IUser;

export class UserDto {
  _id;
  role;

  phone;
  state;
  nickname;
  gender;
  city;
  country;
  events;
  dialogs;

  photos;
  vk;
  instagram;

  constructor(userData: Partial<IUser>) {
    this._id = userData._id;
    this.role = userData.role;

    this.phone = userData.phone;
    this.state = userData.state;
    this.nickname = userData.nickname;
    this.gender = userData.gender;
    this.city = userData.city;
    this.country = userData.country;
    this.events = userData.events;
    this.dialogs = userData.dialogs;

    this.photos = userData.photos;
    this.vk = userData.vk;
    this.instagram = userData.instagram;
  }
}
