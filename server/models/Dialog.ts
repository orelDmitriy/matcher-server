import { Schema, model } from 'mongoose';

import { MongoModels } from '../namespaces/MongoModels';
import IDialog = MongoModels.IDialog;
import { modelsUtils } from '../utils/modelsUtils';

const DialogSchema = new Schema({
  firstUserId: {
    type: String,
    ref: 'user',
    required: true,
    readonly: true,
  },
  secondUserId: {
    type: String,
    ref: 'user',
    required: true,
    readonly: true,
  },
  blocked: [
    {
      type: String,
      ref: 'user',
    },
  ],

  messages: [
    {
      type: String,
      ref: 'message',
    },
  ],

  createAt: { type: Number, default: Date.now, readonly: true },
});

DialogSchema.pre('save', modelsUtils.validateAndPrepareFields);

export const Dialog = model<IDialog>('dialog', DialogSchema);
