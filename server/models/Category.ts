import { Schema, model } from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';

import { MongoModels } from '../namespaces/MongoModels';
import ICategory = MongoModels.ICategory;
import { modelsUtils } from '../utils/modelsUtils';

const CategorySchema = new Schema({
  category: {
    type: String,
    required: true,
    minLength: 3,
    maxLength: 30,
    unique: true,
  },

  createAt: { type: Number, default: Date.now, readonly: true },
});

CategorySchema.plugin(uniqueValidator);
CategorySchema.pre('save', modelsUtils.validateAndPrepareFields);

export const Category = model<ICategory>('category', CategorySchema);
