import { Schema, model } from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';

import { MongoModels } from '../namespaces/MongoModels';
import { modelsUtils } from '../utils/modelsUtils';
import { GlobalModels } from '../namespaces/GlobalModels';
import FollowerState = GlobalModels.FollowerState;
import IEventsFollower = MongoModels.IEventsFollower;

const EventsFollowerSchema: Schema = new Schema({
  userId: {
    type: String,
    ref: 'user',
    required: true,
    readonly: true,
  },

  eventId: {
    type: String,
    ref: 'event',
    required: true,
    readonly: true,
  },

  state: {
    type: String,
    enum: Object.values(FollowerState),
    default: FollowerState.follow,
    required: true,
  },

  createAt: { type: Number, default: Date.now, readonly: true },
  updateAt: { type: Number, default: Date.now },
});

EventsFollowerSchema.plugin(uniqueValidator);
EventsFollowerSchema.pre('save', modelsUtils.validateAndPrepareFields);

export const EventsFollower = model<IEventsFollower>(
  'events_follower',
  EventsFollowerSchema
);
