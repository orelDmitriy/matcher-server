import { Schema, model } from 'mongoose';

import { MongoModels } from '../namespaces/MongoModels';
import IMessage = MongoModels.IMessage;
import { modelsUtils } from '../utils/modelsUtils';

const MessageSchema = new Schema({
  creatorId: {
    type: String,
    ref: 'user',
    required: true,
    readonly: true,
  },
  message: {
    type: String,
    maxLength: 100,
  },
  read: [
    {
      type: String,
      ref: 'user',
    },
  ],

  createAt: { type: Number, default: Date.now, readonly: true },
});

MessageSchema.pre('save', modelsUtils.validateAndPrepareFields);

export const Message = model<IMessage>('message', MessageSchema);
