import { MongoModels } from '../namespaces/MongoModels';
import { objectUtils } from '../utils/objectUtils';
import { UnauthorizedError } from '../errors/UnauthorizedError';
import { Event } from '../models/Event';
import { userService } from './userService';
import { processMongooseErrors } from '../decorators/processMongooseErrors';
import { convertToNotFoundError } from '../decorators/convertToNotFoundError';
import { GlobalModels } from '../namespaces/GlobalModels';
import IEvent = MongoModels.IEvent;
import UserRole = GlobalModels.UserRole;
import IUser = MongoModels.IUser;
import FollowerState = GlobalModels.FollowerState;
import { NotFoundError } from '../errors/NotFoundError';
import Mongoose from 'mongoose';
import { AccessError } from '../errors/AccessError';

class EventService {
  async getEvents(count: number, from: number): Promise<IEvent[]> {
    return Event.find().limit(count).skip(from);
  }

  @convertToNotFoundError()
  async getById(eventId: string): Promise<IEvent> {
    return Event.findById(eventId);
  }

  @convertToNotFoundError()
  async getFullDataById(eventId: string): Promise<IEvent> {
    const event: Array<IEvent> = await Event.aggregate<IEvent>([
      { $match: { _id: new Mongoose.Types.ObjectId(eventId) } },
      {
        $lookup: {
          from: 'events_followers',
          let: { eventId: { $toString: '$_id' } },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ['$eventId', '$$eventId'],
                },
              },
            },
          ],
          as: 'followers',
        },
      },
      {
        $addFields: {
          usersBlocked: {
            $filter: {
              input: '$followers',
              as: 'follower',
              cond: {
                $eq: ['$$follower.state', FollowerState.blocked],
              },
            },
          },
          members: {
            $filter: {
              input: '$followers',
              as: 'follower',
              cond: {
                $eq: ['$$follower.state', FollowerState.member],
              },
            },
          },
          followers: {
            $filter: {
              input: '$followers',
              as: 'follower',
              cond: {
                $eq: ['$$follower.state', FollowerState.follow],
              },
            },
          },
        },
      },
      {
        $addFields: {
          usersBlocked: {
            $map: {
              input: '$usersBlocked',
              as: 'user',
              in: { $toString: '$$user.userId' },
            },
          },
          members: {
            $map: {
              input: '$members',
              as: 'user',
              in: { $toString: '$$user.userId' },
            },
          },
          followers: {
            $map: {
              input: '$followers',
              as: 'user',
              in: { $toString: '$$user.userId' },
            },
          },
        },
      },
    ]);
    if (!event[0]) {
      throw new NotFoundError(`Event with id: ${eventId} doesn't exist`);
    }

    return event[0];
  }

  @processMongooseErrors()
  async create(eventData): Promise<IEvent> {
    let event: IEvent = new Event(eventData);

    event = await event.save();
    await userService.addEvent(eventData.authorId, event._id);

    return event;
  }

  @processMongooseErrors()
  async update(
    eventId: string,
    newEventData: Partial<IEvent>,
    authorId: string
  ): Promise<IEvent> {
    const event: IEvent = await eventService.getById(eventId);
    const author: IUser = await userService.getById(authorId);

    if (!author || String(event.authorId) !== String(author._id)) {
      throw new UnauthorizedError();
    }

    objectUtils.forEach(Event.schema.paths, (fieldValue, fieldName) => {
      if (newEventData.hasOwnProperty(fieldName)) {
        event[fieldName] = newEventData[fieldName];
      }
    });

    return event.save();
  }

  @processMongooseErrors()
  async delete(eventId: string, requesterId: string): Promise<IEvent> {
    let event: IEvent = await eventService.getById(eventId);
    const requester: IUser = await userService.getById(requesterId);

    if (
      !requester ||
      (requester.role !== UserRole.admin &&
        String(event.authorId) !== String(requester._id))
    ) {
      throw new AccessError();
    }

    event = await event.remove();
    await userService.removeEvent(requesterId, eventId);

    return event;
  }
}

export const eventService = new EventService();
