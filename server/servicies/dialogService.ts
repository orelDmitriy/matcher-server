import { Dialog } from '../models/Dialog';
import { processMongooseErrors } from '../decorators/processMongooseErrors';
import { MongoModels } from '../namespaces/MongoModels';
import { convertToNotFoundError } from '../decorators/convertToNotFoundError';
import { FieldsValidationError } from '../errors/FieldsValidationError';
import { ErrorMessage } from '../constants/ErrorMessage';
import { ErrorsModels } from '../namespaces/ErrorsModels';
import { userService } from './userService';
import { GlobalModels } from '../namespaces/GlobalModels';
import { AccessError } from '../errors/AccessError';
import { messageService } from './messageService';
import { IncorrectRequestError } from '../errors/IncorrectRequestError';
import IDialog = MongoModels.IDialog;
import FieldsValidationErrorTypes = ErrorsModels.FieldsValidationErrorTypes;
import IUser = MongoModels.IUser;
import UserRole = GlobalModels.UserRole;
import UserState = GlobalModels.UserState;

class DialogService {
  @convertToNotFoundError()
  async getById(dialogId: string): Promise<IDialog> {
    return Dialog.findById(dialogId);
  }

  @convertToNotFoundError()
  async getByBothUsers(
    firstUserId: string,
    secondUserId: string
  ): Promise<IDialog> {
    const sorted: string[] = [firstUserId, secondUserId].sort();
    const dialogs: Array<IDialog> = await Dialog.find({
      firstUserId: sorted[0],
      secondUserId: sorted[1],
    });

    return dialogs[0];
  }

  @processMongooseErrors()
  async create(requesterId: string, userId: string): Promise<IDialog> {
    const user = await userService.getById(userId);
    if (!user) {
      throw new IncorrectRequestError();
    }

    if (user.state === UserState.blocked) {
      throw new AccessError();
    }

    const sorted: string[] = [requesterId, userId].sort();
    const createdDialog = await this.getByBothUsers(requesterId, userId);
    if (createdDialog) {
      throw new IncorrectRequestError(ErrorMessage.dialogAlreadyExist);
    }

    const dialog: IDialog = new Dialog({
      firstUserId: sorted[0],
      secondUserId: sorted[1],
    });

    return dialog.save();
  }

  @processMongooseErrors()
  async block(dialogId: string, userId: string): Promise<IDialog> {
    const dialog = await this.getById(dialogId);
    const isUserBlocked = dialog.blocked.includes(userId);
    if (
      (dialog.firstUserId !== userId && dialog.secondUserId !== userId) ||
      isUserBlocked
    ) {
      const errorMessage = isUserBlocked
        ? ErrorMessage.dialogAlreadyBlocked
        : ErrorMessage.userIsNotDialogParticipant(userId);
      throw new FieldsValidationError(errorMessage, [
        {
          fieldName: 'userId',
          message: errorMessage,
          type: FieldsValidationErrorTypes.INCORRECT_VALUE,
        },
      ]);
    }

    dialog.blocked.push(userId);

    return dialog.save();
  }

  @processMongooseErrors()
  async unblock(dialogId: string, userId: string): Promise<IDialog> {
    const dialog = await this.getById(dialogId);
    const isUserBlocked = dialog.blocked.includes(userId);
    if (
      (dialog.firstUserId !== userId && dialog.secondUserId !== userId) ||
      !isUserBlocked
    ) {
      const errorMessage = !isUserBlocked
        ? ErrorMessage.userAreNotBlocked
        : ErrorMessage.userIsNotDialogParticipant(userId);
      throw new FieldsValidationError(errorMessage, [
        {
          fieldName: 'userId',
          message: errorMessage,
          type: FieldsValidationErrorTypes.INCORRECT_VALUE,
        },
      ]);
    }

    dialog.blocked = dialog.blocked.filter(
      blockedUserId => blockedUserId !== userId
    );

    return dialog.save();
  }

  @processMongooseErrors()
  async delete(dialogId: string, requesterId: string): Promise<IDialog> {
    const dialog: IDialog = await dialogService.getById(dialogId);
    const requester: IUser = await userService.getById(requesterId);

    if (
      !requester ||
      (requester.role !== UserRole.admin &&
        String(dialog.firstUserId) !== String(requester._id) &&
        String(dialog.secondUserId) !== String(requester._id))
    ) {
      throw new AccessError();
    }

    await messageService.deleteAll(dialog.messages);
    return dialog.remove();
  }

  async getUserDialogs(userId: string): Promise<IDialog[]> {
    return Dialog.find({
      $or: [{ firstUserId: userId }, { secondUserId: userId }],
    });
  }
}

export const dialogService = new DialogService();
