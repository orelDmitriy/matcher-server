import { MongoModels } from '../namespaces/MongoModels';
import { User } from '../models/User';
import { objectUtils } from '../utils/objectUtils';
import { NotFoundError } from '../errors/NotFoundError';
import IUser = MongoModels.IUser;
import { processMongooseErrors } from '../decorators/processMongooseErrors';
import { GlobalModels } from '../namespaces/GlobalModels';
import UserState = GlobalModels.UserState;
import { convertToNotFoundError } from '../decorators/convertToNotFoundError';
import { UsersEvent } from '../models/UsersEvent';
import IUsersEvent = MongoModels.IUsersEvent;
import { inspectObject } from '../utils/inspectObject';
import { UnauthorizedError } from '../errors/UnauthorizedError';
import { ErrorMessage } from '../constants/ErrorMessage';
import logger from '../utils/logger';

class UserService {
  @convertToNotFoundError()
  async getByNickname(nickname: string): Promise<IUser> {
    return User.findOne({ nickname });
  }

  @convertToNotFoundError()
  async getFullDataByNickname(
    nickname: string,
    fullAccess = false
  ): Promise<IUser> {
    const aggregate: any[] = [{ $match: { nickname } }];
    if (fullAccess) {
      aggregate.push(
        {
          $lookup: {
            from: 'users_events',
            let: { userId: { $toString: '$_id' } },
            pipeline: [
              {
                $match: {
                  $expr: {
                    $eq: ['$userId', '$$userId'],
                  },
                },
              },
              {
                $addFields: {
                  eventId: {
                    $toObjectId: '$eventId',
                  },
                },
              },
            ],
            as: 'events',
          },
        },
        {
          $lookup: {
            from: 'events',
            localField: 'events.eventId',
            foreignField: '_id',
            as: 'events',
          },
        },
        {
          $lookup: {
            from: 'dialogs',
            let: { user_item: { $toString: '$_id' } },
            pipeline: [
              {
                $match: {
                  $expr: {
                    $or: [
                      { $eq: ['$firstUserId', '$$user_item'] },
                      { $eq: ['$secondUserId', '$$user_item'] },
                    ],
                  },
                },
              },
            ],
            as: 'dialogs',
          },
        }
      );
    }
    const user: Array<IUser> = await User.aggregate<IUser>(aggregate);

    if (!user[0]) {
      throw new NotFoundError(ErrorMessage.userDoesntExist(nickname));
    }
    return user[0];
  }

  @convertToNotFoundError()
  async getById(userId: string): Promise<IUser> {
    return User.findById(userId);
  }

  @processMongooseErrors()
  async create(userData: Partial<IUser>): Promise<IUser> {
    const user: IUser = new User(userData);
    return await user.save();
  }

  @processMongooseErrors()
  async update(nickname: string, newUserData: Partial<IUser>): Promise<IUser> {
    const user: IUser = await userService.getByNickname(nickname);
    if (!user) {
      throw new NotFoundError(ErrorMessage.userDoesntExist(nickname));
    }
    objectUtils.forEach(user.toObject(), (fieldValue, fieldName) => {
      if (newUserData.hasOwnProperty(fieldName)) {
        user[fieldName] = newUserData[fieldName];
      }
    });
    return await user.save();
  }

  async addEvent(userId: string, eventId: string): Promise<void> {
    const usersEvent: IUsersEvent = new UsersEvent({
      userId,
      eventId,
    });
    await usersEvent.save();
  }

  async removeEvent(userId: string, eventId: string): Promise<void> {
    await UsersEvent.deleteOne({ userId, eventId });
  }

  @processMongooseErrors()
  async delete(nickname: string): Promise<IUser> {
    const user: IUser = await userService.getByNickname(nickname);
    if (!user) {
      throw new NotFoundError(ErrorMessage.userDoesntExist(nickname));
    }
    user.state = UserState.deleted;
    return await user.save();
  }

  async checkCredential(
    requesterId: string,
    requesterToken: string,
    roles?: string[]
  ): Promise<Partial<IUser>> {
    let user: IUser = null;
    try {
      user = await User.findById(requesterId)
        .select(['token', 'state', 'role', 'nickname'])
        .lean();
    } catch (error) {
      logger.warn(error);
    }

    if (!user) {
      throw new UnauthorizedError(
        ErrorMessage.userWithIdDoesntExist(requesterId)
      );
    }

    if (user.token !== requesterToken) {
      throw new UnauthorizedError(ErrorMessage.tokenIsNotRight);
    }

    if (user.state !== UserState.active) {
      throw new UnauthorizedError(
        ErrorMessage.userHasState(user?.nickname, user?.state)
      );
    }

    if (roles?.length && !roles.includes(user.role)) {
      throw new UnauthorizedError(
        ErrorMessage.userMustHaveRole(inspectObject(roles))
      );
    }

    return user;
  }
}

export const userService = new UserService();
