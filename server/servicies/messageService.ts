import { processMongooseErrors } from '../decorators/processMongooseErrors';
import { MongoModels } from '../namespaces/MongoModels';
import IMessage = MongoModels.IMessage;
import { Message } from '../models/Message';
import { convertToNotFoundError } from '../decorators/convertToNotFoundError';
import { AccessError } from '../errors/AccessError';
import { NotFoundError } from '../errors/NotFoundError';
import { IncorrectRequestError } from '../errors/IncorrectRequestError';

class MessageService {
  @processMongooseErrors()
  async create(authorId: string, message: string): Promise<IMessage> {
    const messageObj: IMessage = new Message({
      creatorId: authorId,
      message,
    });

    return await messageObj.save();
  }

  @processMongooseErrors()
  async insertMany(
    messages: Array<{
      creatorId: string;
      message: string;
    }>
  ): Promise<IMessage[]> {
    return Message.insertMany(
      messages.map(messageData => new Message(messageData))
    );
  }

  @convertToNotFoundError()
  async getByIds(ids: string[]): Promise<IMessage[]> {
    return Message.find({
      _id: {
        $in: ids,
      },
    });
  }

  @convertToNotFoundError()
  async getById(messageId: string): Promise<IMessage> {
    return Message.findById(messageId);
  }

  @convertToNotFoundError()
  async deleteAll(messagesIds: string[]): Promise<void> {
    await Message.deleteMany({
      _id: {
        $in: messagesIds,
      },
    });
  }

  checkUserPermission(message: IMessage, userId: string): boolean {
    return Boolean(message.creatorId === userId);
  }

  @processMongooseErrors()
  async update(
    messageId: string,
    requesterId: string,
    message: string,
    read: boolean
  ): Promise<IMessage> {
    const messageObj = await this.getById(messageId);

    if (!messageObj) {
      throw new NotFoundError(`Can't found message with id: ${messageId}`);
    }

    let updated = false;
    if (message?.length) {
      if (!messageService.checkUserPermission(messageObj, requesterId)) {
        throw new AccessError();
      }

      messageObj.message = message;
      updated = true;
    }

    if (read && !messageObj.read.includes(requesterId)) {
      messageObj.read.push(requesterId);
      updated = true;
    }

    if (!updated) {
      throw new IncorrectRequestError();
    }

    return messageObj.save();
  }
}

export const messageService = new MessageService();
