import { processMongooseErrors } from '../decorators/processMongooseErrors';
import { MongoModels } from '../namespaces/MongoModels';
import IMessage = MongoModels.IMessage;
import { dialogService } from './dialogService';
import { AccessError } from '../errors/AccessError';
import { FieldsValidationError } from '../errors/FieldsValidationError';
import { ErrorMessage } from '../constants/ErrorMessage';
import { ErrorsModels } from '../namespaces/ErrorsModels';
import FieldsValidationErrorTypes = ErrorsModels.FieldsValidationErrorTypes;
import { messageService } from './messageService';
import IDialog = MongoModels.IDialog;
import { IncorrectRequestError } from '../errors/IncorrectRequestError';

class DialogMessageService {
  DEFAULT_COUNT_MESSAGE = 15;
  MAX_COUNT_MESSAGE = 100;

  @processMongooseErrors()
  async insertMany(
    dialogId: string,
    messages: Array<{
      creatorId: string;
      message: string;
    }>
  ): Promise<IDialog> {
    const dialog = await dialogService.getById(dialogId);

    const messagesObjs = await messageService.insertMany(messages);
    messagesObjs.forEach(messageObj => {
      if (!this.userHasPermission(dialog, String(messageObj.creatorId))) {
        throw new AccessError();
      }

      dialog.messages.push(String(messageObj._id));
    });

    return dialog.save();
  }

  @processMongooseErrors()
  async create(
    dialogId: string,
    requesterId: string,
    message: string
  ): Promise<IMessage> {
    const dialog = await dialogService.getById(dialogId);

    if (!this.userHasPermission(dialog, requesterId)) {
      throw new AccessError();
    }

    if (!message?.length) {
      throw new FieldsValidationError(ErrorMessage.invalidRequest, [
        {
          fieldName: 'message',
          message: "Message can't be empty",
          type: FieldsValidationErrorTypes.INCORRECT_VALUE,
        },
      ]);
    }

    const messageObj = await messageService.create(requesterId, message);
    dialog.messages.push(messageObj._id);
    await dialog.save();

    return messageObj;
  }

  @processMongooseErrors()
  async delete(
    dialogId: string,
    requesterId: string,
    messageId: string
  ): Promise<void> {
    const dialog = await dialogService.getById(dialogId);

    const indexMessage = dialog.messages.indexOf(messageId);

    if (indexMessage === -1) {
      throw new IncorrectRequestError(
        `Dialog:${dialogId} has not message:${messageId}`
      );
    }

    dialog.messages.splice(indexMessage, 1);

    const messageObj = await messageService.getById(messageId);
    if (!messageService.checkUserPermission(messageObj, requesterId)) {
      throw new AccessError();
    }

    await messageService.deleteAll([messageId]);
    await dialog.save();
  }

  @processMongooseErrors()
  async get(
    dialogId: string,
    requesterId: string,
    pointer: number,
    count: number
  ): Promise<{
    messages: IMessage[];
    pointer: number;
  }> {
    const dialog = await dialogService.getById(dialogId);

    if (!this.userHasPermission(dialog, requesterId)) {
      throw new AccessError();
    }
    const validPointer =
      pointer >= 0 && pointer < dialog.messages.length
        ? pointer
        : dialog.messages.length - 1;
    let validCount =
      count && count > 0 && count <= this.MAX_COUNT_MESSAGE
        ? count
        : this.DEFAULT_COUNT_MESSAGE;

    let nextPointer = validPointer - validCount;

    if (nextPointer < 0) {
      validCount = pointer + 1;
      nextPointer = 0;
    }

    const messagesIds = dialog.messages.slice(
      nextPointer,
      nextPointer + validCount
    );

    const messages = await messageService.getByIds(messagesIds);

    return {
      messages,
      pointer: nextPointer,
    };
  }

  userHasPermission(dialog: IDialog, userId: string): boolean {
    return Boolean(
      !dialog.blocked.includes(userId) &&
        (dialog.firstUserId || dialog.secondUserId)
    );
  }
}

export const dialogMessageService = new DialogMessageService();
