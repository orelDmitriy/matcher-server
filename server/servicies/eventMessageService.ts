import { processMongooseErrors } from '../decorators/processMongooseErrors';
import { MongoModels } from '../namespaces/MongoModels';
import IMessage = MongoModels.IMessage;
import { AccessError } from '../errors/AccessError';
import { FieldsValidationError } from '../errors/FieldsValidationError';
import { ErrorMessage } from '../constants/ErrorMessage';
import { ErrorsModels } from '../namespaces/ErrorsModels';
import FieldsValidationErrorTypes = ErrorsModels.FieldsValidationErrorTypes;
import { messageService } from './messageService';
import { IncorrectRequestError } from '../errors/IncorrectRequestError';
import IEvent = MongoModels.IEvent;
import { eventService } from './eventService';

class EventMessageService {
  DEFAULT_COUNT_MESSAGE = 15;
  MAX_COUNT_MESSAGE = 100;

  @processMongooseErrors()
  async insertMany(
    eventId: string,
    messages: Array<{
      creatorId: string;
      message: string;
    }>
  ): Promise<IEvent> {
    const eventFullData = await eventService.getFullDataById(eventId);

    const messagesObjs = await messageService.insertMany(messages);

    messagesObjs.forEach(messageObj => {
      if (!this.userHasPermission(eventFullData, messageObj.creatorId)) {
        throw new AccessError(
          `Access close. Event.author:${eventFullData.authorId} Event.members:${eventFullData.members} creator:${messageObj.creatorId}`
        );
      }

      eventFullData.messages.push(String(messageObj._id));
    });

    const event = await eventService.getById(eventId);
    event.messages = eventFullData.messages;

    return event.save();
  }

  @processMongooseErrors()
  async create(
    eventId: string,
    requesterId: string,
    message: string
  ): Promise<IMessage> {
    const eventFullData = await eventService.getFullDataById(eventId);

    if (!this.userHasPermission(eventFullData, requesterId)) {
      throw new AccessError(
        `Create event message blocked. 
        eventId: ${eventId}.
        requesterId: ${requesterId}
        event.author: ${eventFullData.authorId}
        event.members: ${eventFullData.members}`
      );
    }

    if (!message?.length) {
      throw new FieldsValidationError(ErrorMessage.invalidRequest, [
        {
          fieldName: 'message',
          message: "Message can't be empty",
          type: FieldsValidationErrorTypes.INCORRECT_VALUE,
        },
      ]);
    }

    const messageObj = await messageService.create(requesterId, message);
    const event = await eventService.getById(eventId);
    event.messages.push(messageObj._id);
    await event.save();

    return messageObj;
  }

  @processMongooseErrors()
  async delete(
    eventId: string,
    requesterId: string,
    messageId: string
  ): Promise<IEvent> {
    const event = await eventService.getById(eventId);

    const indexMessage = event.messages.indexOf(messageId);

    if (indexMessage === -1) {
      throw new IncorrectRequestError(
        `Event:${eventId} has not message:${messageId}`
      );
    }

    event.messages.splice(indexMessage, 1);

    const messageObj = await messageService.getById(messageId);
    if (!messageService.checkUserPermission(messageObj, requesterId)) {
      throw new AccessError(
        `Can't delete message because requester: ${requesterId} is not author message ${messageObj._id}`
      );
    }

    await messageService.deleteAll([messageId]);
    return event.save();
  }

  @processMongooseErrors()
  async get(
    eventId: string,
    requesterId: string,
    pointer: number,
    count: number
  ): Promise<{
    messages: IMessage[];
    pointer: number;
  }> {
    const event = await eventService.getFullDataById(eventId);

    if (!this.userHasPermission(event, requesterId)) {
      throw new AccessError();
    }
    const validPointer =
      pointer >= 0 && pointer < event.messages.length
        ? pointer
        : event.messages.length - 1;
    let validCount =
      count && count > 0 && count <= this.MAX_COUNT_MESSAGE
        ? count
        : this.DEFAULT_COUNT_MESSAGE;

    let nextPointer = validPointer - validCount;

    if (nextPointer < 0) {
      validCount = pointer + 1;
      nextPointer = 0;
    }

    const messagesIds = event.messages.slice(
      nextPointer,
      nextPointer + validCount
    );

    const messages = await messageService.getByIds(messagesIds);

    return {
      messages,
      pointer: nextPointer,
    };
  }

  userHasPermission(event: IEvent, userId: string) {
    return event.authorId === userId || event?.members?.includes(userId);
  }
}

export const eventMessageService = new EventMessageService();
