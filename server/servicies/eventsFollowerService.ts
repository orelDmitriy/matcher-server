import { processMongooseErrors } from '../decorators/processMongooseErrors';
import { EventsFollower } from '../models/EventsFollower';
import { GlobalModels } from '../namespaces/GlobalModels';
import FollowerState = GlobalModels.FollowerState;
import { NotFoundError } from '../errors/NotFoundError';
import { MongoModels } from '../namespaces/MongoModels';
import IEventsFollower = MongoModels.IEventsFollower;
import { ErrorMessage } from '../constants/ErrorMessage';

class EventsFollowerService {
  @processMongooseErrors()
  async addFollower(eventId: string, userId: string): Promise<void> {
    const eventsFollower = new EventsFollower({
      userId,
      eventId,
      state: FollowerState.follow,
    });

    await eventsFollower.save();
  }

  async changeFollowerState(
    eventId: string,
    followerId: string,
    state: FollowerState
  ): Promise<void> {
    const eventsFollower: IEventsFollower = await EventsFollower.findOne({
      eventId,
      userId: followerId,
    });

    if (!eventsFollower) {
      throw new NotFoundError(
        ErrorMessage.eventHasNotFollower(eventId, followerId)
      );
    }

    eventsFollower.state = state;
    await eventsFollower.save();
  }

  @processMongooseErrors()
  async removeFollower(eventId: string, followerId: string): Promise<void> {
    await EventsFollower.deleteOne({ userId: followerId, eventId });
  }

  @processMongooseErrors()
  async getAllFollowersByEventId(eventId: string): Promise<IEventsFollower[]> {
    return EventsFollower.find({ eventId });
  }
}

export const eventsFollowerService = new EventsFollowerService();
