import { Category } from '../models/Category';
import { MongoModels } from '../namespaces/MongoModels';
import ICategory = MongoModels.ICategory;
import { processMongooseErrors } from '../decorators/processMongooseErrors';
import { convertToNotFoundError } from '../decorators/convertToNotFoundError';

class CategoryService {
  @processMongooseErrors()
  async create(categoryData: Partial<ICategory>): Promise<ICategory> {
    const category: ICategory = new Category(categoryData);
    return category.save();
  }

  @processMongooseErrors()
  async delete(categoryId: string): Promise<ICategory> {
    return Category.findByIdAndRemove(categoryId);
  }

  async getAll(): Promise<ICategory[]> {
    return Category.find();
  }

  @convertToNotFoundError()
  async getById(categoryId: string): Promise<ICategory> {
    return Category.findById(categoryId);
  }
}

export const categoryService = new CategoryService();
