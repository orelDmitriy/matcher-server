export namespace GlobalModels {
  export enum UserRole {
    admin = 'admin',
    user = 'user',
  }

  export enum Gender {
    male = 'male',
    female = 'female',
  }

  export enum UserState {
    active = 'active',
    blocked = 'blocked',
    deleted = 'deleted',
  }

  export enum EventState {
    opened = 'opened',
    closed = 'closed',
  }

  export enum EventFor {
    male = 'male',
    female = 'female',
    any = 'any',
  }

  export enum FollowerState {
    follow = 'follow',
    blocked = 'blocked',
    member = 'member',
  }
}
