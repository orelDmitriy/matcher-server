import { ResponseCreator } from '../configs/webSocket/ResponseCreator';
import { ErrorsModels } from './ErrorsModels';
import { MongoModels } from './MongoModels';
import { GlobalModels } from './GlobalModels';
import { UserDto } from '../models/dto/UserDto';

export namespace WsModels {
  import IMessage = MongoModels.IMessage;
  import IDialog = MongoModels.IDialog;
  import IEvent = MongoModels.IEvent;
  import UserRole = GlobalModels.UserRole;
  import ICategory = MongoModels.ICategory;

  export enum RequestMethod {
    GET = 'GET',
    POST = 'POST',
    DELETE = 'DELETE',
    PUT = 'PUT',
    PATCH = 'PATCH',
    HEAD = 'HEAD',
    CONNECT = 'CONNECT',
    OPTIONS = 'OPTIONS',
    TRACE = 'TRACE',
  }

  export interface RequestData {
    actionId: string;
    path: string;
    method: RequestMethod;
    query?: {
      limit?: string;
      from?: string;
    };
    params?: {
      nickname?: string;
      eventId?: string;
      categoryId?: string;
    };
    headers?: {
      'auth-requester-id'?: string;
      'auth-requester-token'?: string;
    };
    body: {
      anon?: boolean;
      for?: GlobalModels.EventFor;
      maxPerson?: number;
      city?: string;
      photos?: string[];
      description?: string;
      useTime?: boolean;
      date?: number;
      token?: string;
      userId?: string;
      dialogId?: string;
      eventId?: string;
      messageId?: string;
      blockUserId?: string;
      unblockUserId?: string;
      pointer?: number;
      count?: number;
      message?: string;
      read?: boolean;
      nickname?: string;
      category?: string;
      checkedPermission?: {
        authorized: boolean;
        nickname?: string;
        requesterId: string;
        token: string;
        role: UserRole;
      };
    };
  }

  export interface TransmitterRequestData {
    path: string;
    method: RequestMethod;
    body: {
      messages?: IMessage[];
      message?: IMessage;
      messageId?: string;
      dialog?: IDialog;
      dialogId?: string;
      event?: IEvent;
      eventId?: string;
    };
  }

  export interface WsControllerMethod {
    (req: RequestData, res: ResponseCreator): any;
  }

  export interface WsController {
    get?: WsControllerMethod;
    post?: WsControllerMethod;
    update?: WsControllerMethod;
    delete?: WsControllerMethod;
    patch?: WsControllerMethod;
  }

  export interface TransmitterControllerMethod {
    (
      reqData: RequestData,
      resData: ResponseData,
      send: (data: TransmitterRequestData, usersIds: string[]) => void
    ): void;
  }

  export interface TransmitterController {
    get?: TransmitterControllerMethod;
    post?: TransmitterControllerMethod;
    update?: TransmitterControllerMethod;
    delete?: TransmitterControllerMethod;
    patch?: TransmitterControllerMethod;
  }

  export interface ResponseBody {
    errors?: ErrorsModels.FieldError[];
    messages?: IMessage[];
    message?: IMessage;
    dialog?: IDialog;
    dialogs?: IDialog[];
    user?: UserDto;
    event?: IEvent;
    events?: IEvent[];
    errorMessage?: string;
    pointer?: number;
    category?: ICategory;
    categories?: ICategory[];
  }

  export interface ResponseData {
    actionId: string;
    status: number;
    body: ResponseBody;
    until?: Date;
  }
}
