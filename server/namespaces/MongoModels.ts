import { Document } from 'mongoose';
import { GlobalModels } from './GlobalModels';

export namespace MongoModels {
  export interface ICategory extends Document {
    _id: string;

    category: string;

    createAt: number;
  }

  export interface IDialog extends Document {
    _id: string;

    firstUserId: IUser['_id'];
    secondUserId: IUser['_id'];
    blocked: Array<IUser['_id']>;
    messages: Array<IMessage['_id']>;

    createAt: number;
  }

  export interface IEvent extends Document {
    _id: string;

    authorId: IUser['_id'];
    members: Array<IUser['_id']>;
    followers: Array<IUser['_id']>;
    usersBlocked: Array<IUser['_id']>;
    messages: Array<IMessage['_id']>;

    categoryId: ICategory['_id'];
    date: number;
    useTime: boolean;
    description: string;
    photos: string[];
    city: string;
    maxPerson: number;
    for: GlobalModels.EventFor;
    anon: boolean;
    state: GlobalModels.EventState;

    createAt: number;
    updateAt: number;
  }

  export interface IFollow extends Document {
    _id: string;

    userId: IUser['_id'];
    message: string;
    showed: boolean;

    createAt: number;
  }

  export interface IMessage extends Document {
    _id: string;

    creatorId: IUser['_id'];
    message: string;
    read: Array<IUser['_id']>;

    createAt: number;
  }

  export interface IUser extends Document {
    _id: string;
    token: string;
    role: GlobalModels.UserRole;

    phone: number;
    state: GlobalModels.UserState;
    nickname: string;
    gender: GlobalModels.Gender;
    city: string;
    country: string;
    events?: Array<IEvent>;
    dialogs?: Array<IDialog>;

    photos: string[];
    vk: string;
    instagram: string;

    createAt: number;
    updateAt: number;
    lastActivity: number;
  }

  export interface IUsersEvent extends Document {
    _id: string;
    userId: string;
    eventId: string;
  }

  export interface IEventsFollower extends Document {
    _id: string;
    eventId: string;
    userId: string;
    state: GlobalModels.FollowerState;
  }
}
