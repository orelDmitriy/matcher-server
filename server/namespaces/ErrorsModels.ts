export namespace ErrorsModels {
  export enum FieldsValidationErrorTypes {
    READONLY = 'readonly',
    FOREIGN_KEY = 'foreignKey',
    INCORRECT_VALUE = 'incorrect_value',
  }

  export interface FieldError {
    fieldName: string;
    message?: string;
    type: string;
  }

  export interface ResponseErrorData {
    errorMessage: string;
    errors?: FieldError[];
    until?: number;
  }
}
