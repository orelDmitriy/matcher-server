import { MongoModels } from '../namespaces/MongoModels';
import ICategory = MongoModels.ICategory;
import { categoryService } from '../servicies/categoryService';
import { processErrors } from '../decorators/processErrors';
import { WsModels } from '../namespaces/WsModels';
import RequestData = WsModels.RequestData;
import { ResponseCreator } from '../configs/webSocket/ResponseCreator';

class Categories {
  @processErrors()
  async get(req: RequestData, res: ResponseCreator): Promise<void> {
    const categories: ICategory[] = await categoryService.getAll();

    res.status(200);
    res.json({ categories });
  }
}

export const categoriesController = new Categories();
