import { MongoModels } from '../namespaces/MongoModels';
import ICategory = MongoModels.ICategory;
import { categoryService } from '../servicies/categoryService';
import { processErrors } from '../decorators/processErrors';
import { checkPermission } from '../decorators/checkPermission';
import { allowedFields } from '../decorators/allowedFields';
import { ResponseCreator } from '../configs/webSocket/ResponseCreator';
import { WsModels } from '../namespaces/WsModels';
import RequestData = WsModels.RequestData;

class Category {
  @allowedFields(['category'])
  @processErrors()
  @checkPermission(['admin'])
  async post(req: RequestData, res: ResponseCreator): Promise<void> {
    const category: ICategory = await categoryService.create(req.body);

    res.status(201);
    res.json({ category });
  }

  @processErrors()
  @checkPermission(['admin'])
  async delete(req: RequestData, res: ResponseCreator): Promise<void> {
    const categoryId: string = req.params.categoryId;
    const category = await categoryService.delete(categoryId);

    res.status(200);
    res.json({ category });
  }
}

export const categoryController = new Category();
