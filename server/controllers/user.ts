import { userService } from '../servicies/userService';
import { MongoModels } from '../namespaces/MongoModels';
import IUser = MongoModels.IUser;
import { processErrors } from '../decorators/processErrors';
import { allowedFields } from '../decorators/allowedFields';
import { checkPermission } from '../decorators/checkPermission';
import { UserDto } from '../models/dto/UserDto';
import { WsModels } from '../namespaces/WsModels';
import RequestData = WsModels.RequestData;
import { ResponseCreator } from '../configs/webSocket/ResponseCreator';

class User {
  @processErrors()
  @checkPermission([], true)
  async get(req: RequestData, res: ResponseCreator): Promise<void> {
    const { nickname: requesterNickname } = req.body.checkedPermission;

    const nickname: string = req.params.nickname;
    const fullAccess = nickname === requesterNickname;

    const user: IUser = await userService.getFullDataByNickname(
      nickname,
      fullAccess
    );

    res.json({ user: new UserDto(user) });
  }

  @allowedFields([
    'token',
    'nickname',
    'phone',
    'gender',
    'city',
    'country',
    'photos',
  ])
  @processErrors()
  async post(req: RequestData, res: ResponseCreator): Promise<void> {
    const user: IUser = await userService.create(req.body);

    res.status(201);
    res.json({ user: new UserDto(user) });
  }

  @allowedFields([
    'token',
    'gender',
    'city',
    'country',
    'photos',
    'vk',
    'instagram',
  ])
  @processErrors()
  @checkPermission()
  async patch(req: RequestData, res: ResponseCreator): Promise<void> {
    const nickname: string = req.params.nickname;

    const user: IUser = await userService.update(nickname, req.body);

    res.json({ user: new UserDto(user) });
  }

  @allowedFields(['state'])
  @processErrors()
  @checkPermission(['admin'])
  async put(req: RequestData, res: ResponseCreator): Promise<void> {
    const nickname: string = req.params.nickname;

    const user: IUser = await userService.update(nickname, req.body);

    res.json({ user: new UserDto(user) });
  }

  @processErrors()
  @checkPermission()
  async delete(req: RequestData, res: ResponseCreator): Promise<void> {
    const nickname: string = req.params.nickname;

    const user: IUser = await userService.delete(nickname);

    res.json({ user: new UserDto(user) });
  }
}
export const userController = new User();
