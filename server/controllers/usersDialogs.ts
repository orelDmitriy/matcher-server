import { userService } from '../servicies/userService';
import { MongoModels } from '../namespaces/MongoModels';
import IUser = MongoModels.IUser;
import { processErrors } from '../decorators/processErrors';
import { checkPermission } from '../decorators/checkPermission';
import { UnauthorizedError } from '../errors/UnauthorizedError';
import { ErrorMessage } from '../constants/ErrorMessage';
import { ResponseCreator } from '../configs/webSocket/ResponseCreator';
import { WsModels } from '../namespaces/WsModels';
import RequestData = WsModels.RequestData;

class UsersDialogs {
  @processErrors()
  @checkPermission()
  async get(req: RequestData, res: ResponseCreator): Promise<void> {
    const { nickname: requesterNickname } = req.body.checkedPermission;

    const nickname: string = req.params.nickname;
    const fullAccess = nickname === requesterNickname;

    if (!fullAccess) {
      throw new UnauthorizedError(ErrorMessage.permissionDenied);
    }
    const user: IUser = await userService.getFullDataByNickname(
      nickname,
      fullAccess
    );

    res.json({ dialogs: user.dialogs });
  }
}
export const usersDialogsController = new UsersDialogs();
