import { eventService } from '../servicies/eventService';
import { MongoModels } from '../namespaces/MongoModels';
import IEvent = MongoModels.IEvent;
import { processErrors } from '../decorators/processErrors';
import { ResponseCreator } from '../configs/webSocket/ResponseCreator';
import { WsModels } from '../namespaces/WsModels';
import RequestData = WsModels.RequestData;

class Events {
  @processErrors()
  async get(req: RequestData, res: ResponseCreator): Promise<void> {
    const limit: string = (req.query.limit || '10') as string;
    const from: string = (req.query.from || '0') as string;

    let processedLimit: number = Number.parseInt(limit, 10);
    let processedFrom: number = Number.parseInt(from, 10);
    processedLimit =
      processedLimit < 1 ? 10 : processedLimit > 100 ? 100 : processedLimit;
    processedFrom = processedFrom >= 0 ? processedFrom : 0;

    const events: IEvent[] = await eventService.getEvents(
      processedLimit,
      processedFrom
    );

    res.json({ events });
  }
}

export const eventsController = new Events();
