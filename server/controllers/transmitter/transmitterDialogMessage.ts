import { WsModels } from '../../namespaces/WsModels';
import TransmitterControllerMethod = WsModels.TransmitterControllerMethod;
import { dialogService } from '../../servicies/dialogService';

class TransmitterDialogMessage {
  post: TransmitterControllerMethod = async (reqData, resData, send) => {
    const dialog = await dialogService.getById(reqData.body.dialogId);
    const recipient =
      dialog.firstUserId === reqData.body.checkedPermission.requesterId
        ? dialog.secondUserId
        : dialog.firstUserId;

    if (resData.status === 201) {
      send(
        {
          path: reqData.path,
          method: reqData.method,
          body: {
            dialogId: reqData.body.dialogId,
            message: resData.body.message,
          },
        },
        [recipient]
      );
    }
  };

  delete: TransmitterControllerMethod = async (reqData, resData, send) => {
    const dialog = await dialogService.getById(reqData.body.dialogId);

    const recipient =
      dialog.firstUserId === reqData.body.checkedPermission.requesterId
        ? dialog.secondUserId
        : dialog.firstUserId;

    if (resData.status === 200) {
      send(
        {
          path: reqData.path,
          method: reqData.method,
          body: {
            dialogId: reqData.body.dialogId,
            messageId: reqData.body.messageId,
          },
        },
        [recipient]
      );
    }
  };
}

export const transmitterDialogMessageController = new TransmitterDialogMessage();
