import { WsModels } from '../../namespaces/WsModels';
import TransmitterControllerMethod = WsModels.TransmitterControllerMethod;
import { eventService } from '../../servicies/eventService';

class TransmitterEventMessage {
  post: TransmitterControllerMethod = async (reqData, resData, send) => {
    const {
      eventId,
      checkedPermission: { requesterId },
    } = reqData.body;
    const recipients = [];
    const event = await eventService.getFullDataById(eventId);

    if (event) {
      if (event.authorId !== requesterId) {
        recipients.push(event.authorId);
      }

      event.members.forEach(memberId => {
        if (memberId !== requesterId) {
          recipients.push(memberId);
        }
      });
    }

    if (resData.status === 201) {
      send(
        {
          path: reqData.path,
          method: reqData.method,
          body: {
            eventId: reqData.body.eventId,
            message: resData.body.message,
          },
        },
        recipients
      );
    }
  };

  delete: TransmitterControllerMethod = async (reqData, resData, send) => {
    const {
      eventId,
      checkedPermission: { requesterId },
    } = reqData.body;
    const recipients = [];
    const event = await eventService.getFullDataById(eventId);

    if (event) {
      if (event.authorId !== requesterId) {
        recipients.push(event.authorId);
      }

      event.members.forEach(memberId => {
        if (memberId !== requesterId) {
          recipients.push(memberId);
        }
      });
    }

    if (resData.status === 200) {
      send(
        {
          path: reqData.path,
          method: reqData.method,
          body: {
            eventId: reqData.body.eventId,
            messageId: reqData.body.messageId,
          },
        },
        recipients
      );
    }
  };
}

export const transmitterEventMessageController = new TransmitterEventMessage();
