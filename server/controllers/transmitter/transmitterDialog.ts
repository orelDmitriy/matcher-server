import { WsModels } from '../../namespaces/WsModels';
import TransmitterControllerMethod = WsModels.TransmitterControllerMethod;

class TransmitterDialog {
  post: TransmitterControllerMethod = (reqData, resData, send) => {
    if (resData.status === 201) {
      send(
        {
          path: reqData.path,
          method: reqData.method,
          body: {
            dialog: resData.body.dialog,
          },
        },
        [reqData.body.userId]
      );
    }
  };

  patch: TransmitterControllerMethod = (reqData, resData, send) => {
    if (resData.status === 200) {
      send(
        {
          path: reqData.path,
          method: reqData.method,
          body: {
            dialog: resData.body.dialog,
          },
        },
        [reqData.body.blockUserId, reqData.body.unblockUserId]
      );
    }
  };

  delete: TransmitterControllerMethod = (reqData, resData, send) => {
    const dialog = resData.body.dialog;
    const recipient =
      dialog.firstUserId === reqData.body.checkedPermission.requesterId
        ? dialog.secondUserId
        : dialog.firstUserId;

    if (resData.status === 200) {
      send(
        {
          path: reqData.path,
          method: reqData.method,
          body: {
            dialogId: reqData.body.dialogId,
          },
        },
        [recipient]
      );
    }
  };
}

export const transmitterDialogController = new TransmitterDialog();
