import { WsModels } from '../../namespaces/WsModels';
import TransmitterControllerMethod = WsModels.TransmitterControllerMethod;
import { dialogService } from '../../servicies/dialogService';
import { eventService } from '../../servicies/eventService';

class TransmitterMessage {
  patch: TransmitterControllerMethod = async (reqData, resData, send) => {
    const {
      dialogId,
      eventId,
      checkedPermission: { requesterId },
    } = reqData.body;
    const recipients = [];

    if (dialogId) {
      const dialog = await dialogService.getById(dialogId);
      const recipient =
        dialog.firstUserId === requesterId
          ? dialog.secondUserId
          : dialog.firstUserId;

      recipients.push(recipient);
    }

    if (eventId) {
      const event = await eventService.getFullDataById(eventId);
      if (event.authorId !== requesterId) {
        recipients.push(event.authorId);
      }

      event.members.forEach(memberId => {
        if (memberId !== requesterId) {
          recipients.push(memberId);
        }
      });
    }

    if (resData.status === 200) {
      send(
        {
          path: reqData.path,
          method: reqData.method,
          body: {
            dialogId: reqData.body.dialogId,
            message: resData.body.message,
          },
        },
        recipients
      );
    }
  };
}

export const transmitterMessageController = new TransmitterMessage();
