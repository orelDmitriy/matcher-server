import { WsModels } from '../../namespaces/WsModels';
import TransmitterControllerMethod = WsModels.TransmitterControllerMethod;

class TransmitterEvent {
  post: TransmitterControllerMethod = (reqData, resData, send) => {
    console.log(reqData);
    console.log(resData);
  };
}

export const transmitterEventController = new TransmitterEvent();
