import { userService } from '../servicies/userService';
import { MongoModels } from '../namespaces/MongoModels';
import IUser = MongoModels.IUser;
import { processErrors } from '../decorators/processErrors';
import { checkPermission } from '../decorators/checkPermission';
import { ResponseCreator } from '../configs/webSocket/ResponseCreator';
import { WsModels } from '../namespaces/WsModels';
import RequestData = WsModels.RequestData;

class UsersEvents {
  @processErrors()
  @checkPermission([], true)
  async get(req: RequestData, res: ResponseCreator): Promise<void> {
    const { nickname: requesterNickname } = req.body.checkedPermission;

    const nickname: string = req.params.nickname;
    const fullAccess = nickname === requesterNickname;

    const user: IUser = await userService.getFullDataByNickname(
      nickname,
      fullAccess
    );

    res.json({ events: user.events });
  }
}
export const usersEventsController = new UsersEvents();
