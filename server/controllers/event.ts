import { eventService } from '../servicies/eventService';
import { MongoModels } from '../namespaces/MongoModels';
import IEvent = MongoModels.IEvent;
import { processErrors } from '../decorators/processErrors';
import { allowedFields } from '../decorators/allowedFields';
import { checkPermission } from '../decorators/checkPermission';
import { debounce } from '../decorators/debounce';
import { ResponseCreator } from '../configs/webSocket/ResponseCreator';
import { WsModels } from '../namespaces/WsModels';
import RequestData = WsModels.RequestData;

class Event {
  @processErrors()
  async get(req: RequestData, res: ResponseCreator): Promise<void> {
    const eventId: string = req.params.eventId;

    const event: IEvent = await eventService.getFullDataById(eventId);

    res.json({ event });
  }

  @allowedFields([
    'categoryId',
    'date',
    'useTime',
    'description',
    'photos',
    'city',
    'maxPerson',
    'for',
    'anon',
  ])
  @processErrors()
  @checkPermission()
  @debounce(6 * 60 * 60 * 1000)
  async post(req: RequestData, res: ResponseCreator): Promise<void> {
    const event: IEvent = await eventService.create({
      ...req.body,
      authorId: req.body.checkedPermission?.requesterId,
    });

    res.status(201);
    res.json({ event });
  }

  @allowedFields([
    'categoryId',
    'date',
    'useTime',
    'description',
    'photos',
    'city',
    'maxPerson',
    'for',
    'anon',
  ])
  @processErrors()
  @checkPermission()
  async patch(req: RequestData, res: ResponseCreator): Promise<void> {
    const eventId: string = req.params.eventId;
    const { checkedPermission, ...eventData } = req.body;

    const event = await eventService.update(
      eventId,
      eventData,
      checkedPermission?.requesterId
    );

    res.json({ event });
  }

  @processErrors()
  @checkPermission()
  async delete(req: RequestData, res: ResponseCreator): Promise<void> {
    const eventId: string = req.params.eventId;

    const event = await eventService.delete(
      eventId,
      req.body.checkedPermission?.requesterId
    );

    res.json({ event });
  }
}

export const eventController = new Event();
