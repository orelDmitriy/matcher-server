import { WsModels } from '../namespaces/WsModels';
import { processErrors } from '../decorators/processErrors';
import { ResponseCreator } from '../configs/webSocket/ResponseCreator';
import RequestData = WsModels.RequestData;
import { checkPermission } from '../decorators/checkPermission';
import { eventMessageService } from '../servicies/eventMessageService';

class EventMessage {
  @processErrors()
  @checkPermission()
  async post(req: RequestData, response: ResponseCreator) {
    const {
      eventId,
      message,
      checkedPermission: { requesterId },
    } = req.body;

    const messageObj = await eventMessageService.create(
      eventId,
      requesterId,
      message
    );

    response.status(201);
    response.json({ message: messageObj });
  }

  @processErrors()
  @checkPermission()
  async get(req: RequestData, response: ResponseCreator) {
    const {
      eventId,
      pointer,
      count,
      checkedPermission: { requesterId },
    } = req.body;

    const { messages, pointer: nextPointer } = await eventMessageService.get(
      eventId,
      requesterId,
      pointer,
      count
    );

    response.status(200);
    response.json({ messages, pointer: nextPointer });
  }

  @processErrors()
  @checkPermission()
  async delete(req: RequestData, response: ResponseCreator) {
    const {
      eventId,
      messageId,
      checkedPermission: { requesterId },
    } = req.body;

    await eventMessageService.delete(eventId, requesterId, messageId);

    response.status(200);
    response.json({});
  }
}

export const wsEventMessageController = new EventMessage();
