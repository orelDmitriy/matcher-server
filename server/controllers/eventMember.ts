import { eventService } from '../servicies/eventService';
import { processErrors } from '../decorators/processErrors';
import { allowedFields } from '../decorators/allowedFields';
import { checkPermission } from '../decorators/checkPermission';
import { MongoModels } from '../namespaces/MongoModels';
import { userService } from '../servicies/userService';
import { GlobalModels } from '../namespaces/GlobalModels';
import { eventsFollowerService } from '../servicies/eventsFollowerService';
import { AccessError } from '../errors/AccessError';
import IUser = MongoModels.IUser;
import FollowerState = GlobalModels.FollowerState;
import EventState = GlobalModels.EventState;
import { FieldsValidationError } from '../errors/FieldsValidationError';
import { ErrorMessage } from '../constants/ErrorMessage';
import { ResponseCreator } from '../configs/webSocket/ResponseCreator';
import { WsModels } from '../namespaces/WsModels';
import RequestData = WsModels.RequestData;

class EventMember {
  @allowedFields(['nickname'])
  @processErrors()
  @checkPermission()
  async post(req: RequestData, res: ResponseCreator): Promise<void> {
    const eventId: string = req.params.eventId;
    const nickname = req.body.nickname;
    const requesterId: string = req.body.checkedPermission?.requesterId;

    const user: IUser = await userService.getByNickname(nickname);
    const eventFullData = await eventService.getFullDataById(eventId);

    if (
      eventFullData.authorId !== requesterId ||
      eventFullData.state === EventState.closed ||
      eventFullData.maxPerson <= eventFullData.members.length
    ) {
      throw new AccessError();
    }

    if (!user) {
      throw new FieldsValidationError(ErrorMessage.userDoesntExist(nickname), [
        {
          fieldName: 'nickname',
          message: ErrorMessage.userDoesntExist(nickname),
          type: FieldsValidationError.errorTypes.INCORRECT_VALUE,
        },
      ]);
    }

    if (
      !eventFullData.followers.find(
        followerId => followerId === String(user._id)
      )
    ) {
      throw new FieldsValidationError(ErrorMessage.userAreNotFollower, [
        {
          fieldName: 'nickname',
          message: ErrorMessage.userAreNotFollower,
          type: FieldsValidationError.errorTypes.INCORRECT_VALUE,
        },
      ]);
    }

    await eventsFollowerService.changeFollowerState(
      eventId,
      String(user._id),
      FollowerState.member
    );

    res.status(204);
    res.send();
  }

  @processErrors()
  @checkPermission()
  async delete(req: RequestData, res: ResponseCreator): Promise<void> {
    const eventId = req.params.eventId;
    const nickname = req.body.nickname;
    const requesterId: string = req.body.checkedPermission?.requesterId;

    const user: IUser = await userService.getByNickname(nickname);
    const eventFullData = await eventService.getFullDataById(eventId);

    if (
      eventFullData.authorId !== requesterId ||
      eventFullData.state === EventState.closed
    ) {
      throw new AccessError();
    }

    if (!user) {
      throw new FieldsValidationError(ErrorMessage.userDoesntExist(nickname), [
        {
          fieldName: 'nickname',
          message: ErrorMessage.userDoesntExist(nickname),
          type: FieldsValidationError.errorTypes.INCORRECT_VALUE,
        },
      ]);
    }

    if (
      !eventFullData.members.find(memberId => memberId === String(user._id))
    ) {
      throw new FieldsValidationError(ErrorMessage.userAreNotMember, [
        {
          fieldName: 'nickname',
          message: ErrorMessage.userAreNotMember,
          type: FieldsValidationError.errorTypes.INCORRECT_VALUE,
        },
      ]);
    }

    await eventsFollowerService.changeFollowerState(
      eventId,
      String(user._id),
      FollowerState.follow
    );

    res.status(204);
    res.send();
  }
}

export const eventMemberController = new EventMember();
