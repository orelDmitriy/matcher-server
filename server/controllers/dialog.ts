import { WsModels } from '../namespaces/WsModels';
import { processErrors } from '../decorators/processErrors';
import { ResponseCreator } from '../configs/webSocket/ResponseCreator';
import RequestData = WsModels.RequestData;
import { dialogService } from '../servicies/dialogService';
import { checkPermission } from '../decorators/checkPermission';
import { AccessError } from '../errors/AccessError';
import { IncorrectRequestError } from '../errors/IncorrectRequestError';

class Dialog {
  @processErrors()
  @checkPermission()
  async post(req: RequestData, response: ResponseCreator) {
    const {
      userId,
      checkedPermission: { requesterId },
    } = req.body;

    const dialog = await dialogService.create(requesterId, userId);

    response.status(201);
    response.json({ dialog });
  }

  @processErrors()
  @checkPermission()
  async patch(req: RequestData, response: ResponseCreator) {
    const {
      dialogId,
      blockUserId,
      unblockUserId,
      checkedPermission: { requesterId },
    } = req.body;

    let dialog = await dialogService.getByBothUsers(
      requesterId,
      blockUserId || unblockUserId
    );
    if (!dialog) {
      throw new AccessError();
    }

    if (blockUserId) {
      dialog = await dialogService.block(dialogId, blockUserId);
    } else if (unblockUserId) {
      dialog = await dialogService.unblock(dialogId, unblockUserId);
    } else {
      throw new IncorrectRequestError();
    }

    response.status(200);
    response.json({ dialog });
  }

  @processErrors()
  @checkPermission()
  async delete(req: RequestData, response: ResponseCreator) {
    const {
      dialogId,
      checkedPermission: { requesterId },
    } = req.body;

    const dialog = await dialogService.delete(dialogId, requesterId);

    response.status(200);
    response.json({ dialog });
  }
}

export const wsDialogController = new Dialog();
