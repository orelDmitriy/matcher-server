import { processErrors } from '../decorators/processErrors';
import { checkPermission } from '../decorators/checkPermission';
import { dialogService } from '../servicies/dialogService';
import { ResponseCreator } from '../configs/webSocket/ResponseCreator';
import { WsModels } from '../namespaces/WsModels';
import RequestData = WsModels.RequestData;

class Dialogs {
  @processErrors()
  @checkPermission()
  async get(req: RequestData, res: ResponseCreator): Promise<void> {
    const {
      checkedPermission: { requesterId },
    } = req.body;

    const dialogs = await dialogService.getUserDialogs(requesterId);

    res.status(200);
    res.json({ dialogs });
  }
}

export const dialogsController = new Dialogs();
