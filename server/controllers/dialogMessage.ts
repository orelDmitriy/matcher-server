import { WsModels } from '../namespaces/WsModels';
import { processErrors } from '../decorators/processErrors';
import { ResponseCreator } from '../configs/webSocket/ResponseCreator';
import RequestData = WsModels.RequestData;
import { checkPermission } from '../decorators/checkPermission';
import { dialogMessageService } from '../servicies/dialogMessageService';

class DialogMessage {
  @processErrors()
  @checkPermission()
  async post(req: RequestData, response: ResponseCreator) {
    const {
      dialogId,
      message,
      checkedPermission: { requesterId },
    } = req.body;

    const messageObj = await dialogMessageService.create(
      dialogId,
      requesterId,
      message
    );

    response.status(201);
    response.json({ message: messageObj });
  }

  @processErrors()
  @checkPermission()
  async get(req: RequestData, response: ResponseCreator) {
    const {
      dialogId,
      pointer,
      count,
      checkedPermission: { requesterId },
    } = req.body;

    const { messages, pointer: nextPointer } = await dialogMessageService.get(
      dialogId,
      requesterId,
      pointer,
      count
    );

    response.status(200);
    response.json({ messages, pointer: nextPointer });
  }

  @processErrors()
  @checkPermission()
  async delete(req: RequestData, response: ResponseCreator) {
    const {
      dialogId,
      messageId,
      checkedPermission: { requesterId },
    } = req.body;

    await dialogMessageService.delete(dialogId, requesterId, messageId);

    response.status(200);
    response.json({});
  }
}

export const wsDialogMessageController = new DialogMessage();
