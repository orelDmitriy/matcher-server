import { WsModels } from '../namespaces/WsModels';
import { processErrors } from '../decorators/processErrors';
import { ResponseCreator } from '../configs/webSocket/ResponseCreator';
import RequestData = WsModels.RequestData;
import { checkPermission } from '../decorators/checkPermission';
import { messageService } from '../servicies/messageService';

class Message {
  @processErrors()
  @checkPermission()
  async patch(req: RequestData, response: ResponseCreator) {
    const {
      messageId,
      message,
      read,
      checkedPermission: { requesterId },
    } = req.body;

    const messageObj = await messageService.update(
      messageId,
      requesterId,
      message,
      read
    );

    response.status(200);
    response.json({ message: messageObj });
  }
}

export const wsMessageController = new Message();
