import { eventService } from '../servicies/eventService';
import { processErrors } from '../decorators/processErrors';
import { checkPermission } from '../decorators/checkPermission';
import { GlobalModels } from '../namespaces/GlobalModels';
import { eventsFollowerService } from '../servicies/eventsFollowerService';
import { AccessError } from '../errors/AccessError';
import EventState = GlobalModels.EventState;
import { FieldsValidationError } from '../errors/FieldsValidationError';
import { ErrorMessage } from '../constants/ErrorMessage';
import { ResponseCreator } from '../configs/webSocket/ResponseCreator';
import { WsModels } from '../namespaces/WsModels';
import RequestData = WsModels.RequestData;

class EventFollower {
  @processErrors()
  @checkPermission()
  async post(req: RequestData, res: ResponseCreator): Promise<void> {
    const eventId = req.params.eventId;
    const userId: string = req.body.checkedPermission?.requesterId;
    const eventFullData = await eventService.getFullDataById(eventId);

    if (
      eventFullData.followers.find(followerId => followerId === userId) ||
      eventFullData.members.find(memberId => memberId === userId) ||
      eventFullData.usersBlocked.find(
        usersBlocked => usersBlocked === userId
      ) ||
      eventFullData.authorId === userId ||
      eventFullData.state === EventState.closed
    ) {
      throw new AccessError();
    }

    await eventsFollowerService.addFollower(eventId, userId);

    res.status(204);
    res.send();
  }

  @processErrors()
  @checkPermission()
  async delete(req: RequestData, res: ResponseCreator): Promise<void> {
    const eventId: string = req.params.eventId;
    const userId: string = req.body.checkedPermission?.requesterId;

    const eventFullData = await eventService.getFullDataById(eventId);

    if (!eventFullData.followers.find(followerId => followerId === userId)) {
      throw new FieldsValidationError(ErrorMessage.userAreNotFollower, [
        {
          fieldName: 'auth-requester-id',
          message: ErrorMessage.userAreNotFollower,
          type: FieldsValidationError.errorTypes.INCORRECT_VALUE,
        },
      ]);
    }

    if (eventFullData.state === EventState.closed) {
      throw new AccessError();
    }

    await eventsFollowerService.removeFollower(eventId, userId);

    res.status(204);
    res.send();
  }
}

export const eventFollowerController = new EventFollower();
