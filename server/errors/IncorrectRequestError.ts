export class IncorrectRequestError extends Error {
  constructor(message = 'Incorrect request') {
    super(message);
  }
}
