import { ErrorsModels } from '../namespaces/ErrorsModels';
import FieldsValidationErrorTypes = ErrorsModels.FieldsValidationErrorTypes;

export class FieldsValidationError extends Error {
  constructor(message: string, private fieldsError: ErrorsModels.FieldError[]) {
    super(message);
  }

  get fieldsErrors() {
    return this.fieldsError;
  }

  static errorTypes = FieldsValidationErrorTypes;
}
