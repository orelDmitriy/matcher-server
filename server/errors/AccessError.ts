import { ErrorMessage } from '../constants/ErrorMessage';

export class AccessError extends Error {
  until: number | null;
  constructor(message = ErrorMessage.accessClosed, until?: number) {
    super(message);
    this.until = until;
  }
}
