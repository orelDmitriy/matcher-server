import { ErrorMessage } from '../constants/ErrorMessage';

export class UnauthorizedError extends Error {
  constructor(message = ErrorMessage.permissionDenied) {
    super(message);
  }
}
