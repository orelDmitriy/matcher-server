import 'mocha';
import { expect } from 'chai';
import request from 'supertest';
import mongoose from 'mongoose';

import { application } from '../../server';
import { categoryService } from '../../server/servicies/categoryService';
import { GlobalModels } from '../../server/namespaces/GlobalModels';
import EventFor = GlobalModels.EventFor;
import { UserFixture } from '../../server/testing/UserFixture';
import { CategoryFixture } from '../../server/testing/CategoryFixture';
import { EventFixture } from '../../server/testing/EventFixture';

describe('event controller', () => {
  const user1 = new UserFixture(1);
  const user2 = new UserFixture(2);
  const admin = new UserFixture(3, true);

  const event1 = new EventFixture(1);
  const event2 = new EventFixture(2);
  const event3 = new EventFixture(3);
  const event4 = new EventFixture(4);
  const category1 = new CategoryFixture(1);
  const category2 = new CategoryFixture(2);
  const deletedCategory = new CategoryFixture(3);
  const nonexistentCategory = new CategoryFixture(4);

  beforeEach(async function () {
    await mongoose.connection.dropDatabase();
    await user1.save();
    await user2.save();
    await admin.save();

    await category1.save();
    await category2.save();
    await deletedCategory.save();
    await categoryService.delete(deletedCategory.getField('_id'));

    await event1.save(user1, category1);
  });

  it('create event with nonexistent category', () => {
    return request(application)
      .post('/api/v1/event')
      .set({
        'auth-requester-token': user1.getField('token'),
        'auth-requester-id': user1.getField('_id'),
      })
      .send({
        ...event2.getDefaultData(),
        categoryId: nonexistentCategory.getField('_id'),
      })
      .expect('Content-Type', /json/)
      .then(response => {
        expect(response.status).to.equals(400);
        expect(response.body.errorMessage).to.be.an('string');
      });
  });

  it('create event with deleted category', () => {
    return request(application)
      .post('/api/v1/event')
      .set({
        'auth-requester-token': user1.getField('token'),
        'auth-requester-id': user1.getField('_id'),
      })
      .send({
        ...event2.getDefaultData(),
        categoryId: deletedCategory.getField('_id'),
      })
      .expect('Content-Type', /json/)
      .then(response => {
        expect(response.status).to.equals(400);
        expect(response.body.errorMessage).to.be.an('string');
      });
  });

  it('create event wrong field', () => {
    return request(application)
      .post('/api/v1/event')
      .set({
        'auth-requester-token': user1.getField('token'),
        'auth-requester-id': user1.getField('_id'),
      })
      .send({
        ...event2.getDefaultData(),
        city: '',
      })
      .expect('Content-Type', /json/)
      .then(response => {
        expect(response.status).to.equals(400);
        expect(response.body.errorMessage).to.be.an('string');
      });
  });

  it('create event with all data', () => {
    return request(application)
      .post('/api/v1/event')
      .set({
        'auth-requester-token': user1.getField('token'),
        'auth-requester-id': user1.getField('_id'),
      })
      .send({
        ...event2.getDefaultData(),
        categoryId: category2.getField('_id'),
      })
      .expect('Content-Type', /json/)
      .then(response => {
        expect(response.status).to.equals(201);
        expect(response.body.event._id).to.be.an('string');
        expect(response.body.event.updateAt).to.be.an('number');
        expect(response.body.event.createAt).to.be.an('number');
      });
  });

  it('create event by user1 without waiting block time', async () => {
    const agent = request(application);
    await agent
      .post('/api/v1/event')
      .set({
        'auth-requester-token': user1.getField('token'),
        'auth-requester-id': user1.getField('_id'),
      })
      .send({
        ...event2.getDefaultData(),
        categoryId: category2.getField('_id'),
      });

    return await agent
      .post('/api/v1/event')
      .set({
        'auth-requester-token': user1.getField('token'),
        'auth-requester-id': user1.getField('_id'),
      })
      .send({
        ...event3.getDefaultData(),
        categoryId: category2.getField('_id'),
      })
      .expect('Content-Type', /json/)
      .then(response => {
        expect(response.status).to.equals(403);
        expect(response.body.errorMessage).to.be.an('string');
        expect(response.body.until).to.be.an('number');
      });
  });

  it('create event by user2', () => {
    return request(application)
      .post('/api/v1/event')
      .set({
        'auth-requester-token': user2.getField('token'),
        'auth-requester-id': user2.getField('_id'),
      })
      .send({
        ...event2.getDefaultData(),
        categoryId: category2.getField('_id'),
      })
      .expect('Content-Type', /json/)
      .then(async response => {
        expect(response.status).to.equals(201);
        expect(response.body.event._id).to.be.an('string');
        event2.updateDefaultId(response.body.event._id);

        const eventInDb = await event2.getDataFromDB();
        expect(eventInDb.authorId).to.equal(user2.getId());

        const userInDb = await user2.getDataFromDB();
        const userEventsIds = userInDb.events.map(event => String(event._id));
        expect(userEventsIds).to.include(event2.getId());
      });
  });

  it('create event with nonexistent user', () => {
    return request(application)
      .post('/api/v1/event')
      .set({
        'auth-requester-token': 'nonexistent requester token',
        'auth-requester-id': 'nonexistent requester id',
      })
      .send({
        ...event2.getDefaultData(),
      })
      .expect('Content-Type', /json/)
      .then(response => {
        expect(response.status).to.equals(401);
        expect(response.body.errorMessage).to.be.an('string');
      });
  });

  it('update readonly fields in events', () => {
    const readonlyFieldsNames = [
      'categoryId',
      'date',
      'useTime',
      'description',
      'photos',
      'city',
      'for',
      'anon',
    ];
    return request(application)
      .patch(`/api/v1/event/${event1.getField('_id')}`)
      .set({
        'auth-requester-token': user1.getField('token'),
        'auth-requester-id': user1.getField('_id'),
      })
      .send({
        categoryId: category2.getField('_id'),
        date: Date.now(),
        useTime: true,
        description: `some event description with id: ${10}`,
        photos: ['234234324'],
        city: `Minsk ${10}`,
        for: EventFor.female,
        anon: true,
      })
      .expect('Content-Type', /json/)
      .then(response => {
        expect(response.status).to.equals(400);
        expect(response.body.errors.length).to.be.equal(
          readonlyFieldsNames.length
        );
        response.body.errors.forEach(error => {
          expect(readonlyFieldsNames).to.include(error.fieldName);
        });
      });
  });

  it('update events maxPerson field', () => {
    return request(application)
      .patch(`/api/v1/event/${event1.getField('_id')}`)
      .set({
        'auth-requester-token': user1.getField('token'),
        'auth-requester-id': user1.getField('_id'),
      })
      .send({
        maxPerson: 21,
      })
      .expect('Content-Type', /json/)
      .then(response => {
        expect(response.status).to.equals(200);
        expect(response.body.event.maxPerson).to.eq(21);
        expect(event1.getField('updateAt')).is.lessThan(
          response.body.event.updateAt
        );
      });
  });

  it('update nonexistent events', () => {
    return request(application)
      .patch(`/api/v1/event/${event4.getField('_id')}`)
      .set({
        'auth-requester-token': user1.getField('token'),
        'auth-requester-id': user1.getField('_id'),
      })
      .send({
        maxPerson: 23,
      })
      .expect('Content-Type', /json/)
      .then(response => {
        expect(response.status).to.equals(404);
        expect(response.body.errorMessage).to.be.an('string');
      });
  });

  it('delete nonexistent events', () => {
    return request(application)
      .delete(`/api/v1/event/${event4.getField('_id')}`)
      .set({
        'auth-requester-token': user1.getField('token'),
        'auth-requester-id': user1.getField('_id'),
      })
      .expect('Content-Type', /json/)
      .then(response => {
        expect(response.status).to.equals(404);
        expect(response.body.errorMessage).to.be.an('string');
      });
  });

  it('delete event by author', () => {
    return request(application)
      .delete(`/api/v1/event/${event1.getField('_id')}`)
      .set({
        'auth-requester-token': user1.getField('token'),
        'auth-requester-id': user1.getField('_id'),
      })
      .expect('Content-Type', /json/)
      .then(response => {
        expect(response.status).to.equals(200);
      });
  });

  it('get event by id', () => {
    return null;
  });
});
