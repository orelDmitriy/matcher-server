import 'mocha';
import { expect } from 'chai';

import mongoose from 'mongoose';
import { WsClientFixture } from '../../server/testing/WsClientFixture';
import { UserFixture } from '../../server/testing/UserFixture';
import { EventFixture } from '../../server/testing/EventFixture';
import { CategoryFixture } from '../../server/testing/CategoryFixture';
import { ApiPath } from '../../server/constants/ApiPath';
import { WsModels } from '../../server/namespaces/WsModels';
import RequestMethod = WsModels.RequestMethod;
import { getRandomInt } from '../../server/utils/utilites';
import { eventMessageService } from '../../server/servicies/eventMessageService';
import { messageService } from '../../server/servicies/messageService';
import { eventService } from '../../server/servicies/eventService';

describe('event message', () => {
  const authorClient = new WsClientFixture();
  const client1 = new WsClientFixture();
  const client2 = new WsClientFixture();
  const client5 = new WsClientFixture();
  const author = new UserFixture(0);
  const user1 = new UserFixture(1);
  const user2 = new UserFixture(2);
  const user5 = new UserFixture(5);
  const event1 = new EventFixture(1);
  const event2 = new EventFixture(2);
  const category = new CategoryFixture(1);

  beforeEach(async () => {
    await mongoose.connection.dropDatabase();
    await mongoose.connection.dropDatabase();
    await user1.save();
    await user2.save();
    await user5.save();
    await author.save();
    authorClient.clear();
    client1.clear();
    client2.clear();
    client5.clear();
    await authorClient.authorize(author);
    await client1.authorize(user1);
    await client2.authorize(user2);
    await client5.authorize(user5);
    await category.save();
    await event1.save(author, category);
    await event1.addMember(user1);
    await event1.addMember(user5);
  });

  describe('event message create', () => {
    it('correct', async () => {
      client1.send({
        actionId: 'eventMessageCreate',
        path: ApiPath.EVENT_MESSAGE,
        method: RequestMethod.POST,
        body: {
          eventId: event1.getId(),
          message: 'some message',
        },
      });

      client1.waitAnswers(1);
      let answers = (await client1.getAnswers())[0];

      expect(answers).to.deep.include({
        actionId: 'eventMessageCreate',
        status: 201,
      });
      expect(answers.body.message._id).to.be.an('string');

      authorClient.waitRequests(1);
      let messageFomServer = (await authorClient.getRequests())[0];

      expect(answers.body.message).to.deep.include(
        messageFomServer.body.message
      );
      expect(event1.getId()).to.deep.include(messageFomServer.body.eventId);

      client5.waitRequests(1);
      messageFomServer = (await client5.getRequests())[0];

      expect(answers.body.message).to.deep.include(
        messageFomServer.body.message
      );
      expect(event1.getId()).to.deep.include(messageFomServer.body.eventId);

      client5.clear();

      authorClient.send({
        actionId: 'eventMessageCreate',
        path: ApiPath.EVENT_MESSAGE,
        method: RequestMethod.POST,
        body: {
          eventId: event1.getId(),
          message: 'another message',
        },
      });

      authorClient.waitAnswers(1);
      answers = (await authorClient.getAnswers())[0];

      expect(answers).to.deep.include({
        actionId: 'eventMessageCreate',
        status: 201,
      });
      expect(answers.body.message._id).to.be.an('string');

      client1.waitRequests(1);
      messageFomServer = (await client1.getRequests())[0];

      expect(answers.body.message).to.deep.include(
        messageFomServer.body.message
      );
      expect(event1.getId()).to.deep.include(messageFomServer.body.eventId);

      client5.waitRequests(1);
      messageFomServer = (await client5.getRequests())[0];

      expect(answers.body.message).to.deep.include(
        messageFomServer.body.message
      );
      expect(event1.getId()).to.deep.include(messageFomServer.body.eventId);
    });

    it('without required data', async () => {
      client1.send({
        actionId: 'eventMessageCreate',
        path: ApiPath.EVENT_MESSAGE,
        method: RequestMethod.POST,
        body: {
          eventId: event1.getId(),
          message: '',
        },
      });

      client1.waitAnswers(1);
      const answers = (await client1.getAnswers())[0];

      expect(answers).to.deep.include({
        actionId: 'eventMessageCreate',
        status: 400,
      });
      expect(answers.body.errorMessage).to.be.an('string');

      authorClient.waitRequests(1);
      let messageFomServer = (await authorClient.getRequests())[0];

      expect(messageFomServer).to.equal(undefined);

      client5.waitRequests(1);
      messageFomServer = (await client5.getRequests())[0];

      expect(messageFomServer).to.equal(undefined);
    });

    it('non-existent event', async () => {
      client1.send({
        actionId: 'eventMessageCreate',
        path: ApiPath.EVENT_MESSAGE,
        method: RequestMethod.POST,
        body: {
          eventId: event2.getId(),
          message: 'some message',
        },
      });

      client1.waitAnswers(1);
      const answers = (await client1.getAnswers())[0];

      expect(answers).to.deep.include({
        actionId: 'eventMessageCreate',
        status: 404,
      });
      expect(answers.body.errorMessage).to.be.an('string');

      authorClient.waitRequests(1);
      let messageFomServer = (await authorClient.getRequests())[0];

      expect(messageFomServer).to.equal(undefined);

      client5.waitRequests(1);
      messageFomServer = (await client5.getRequests())[0];

      expect(messageFomServer).to.equal(undefined);
    });
  });

  describe('get', () => {
    const countMessage = 130;

    beforeEach(async () => {
      const users = [user1, author];
      const messages = [];
      for (let i = 0; i < countMessage; i++) {
        messages.push({
          creatorId: users[i % 2].getId(),
          message: `Message ${i}.`,
        });
      }

      await event1.addAllMessages(messages);
    });

    it('with different counts and result types', async () => {
      const differentCountsTypes = [
        [getRandomInt(1, eventMessageService.MAX_COUNT_MESSAGE)],
        [eventMessageService.MAX_COUNT_MESSAGE],
        [
          undefined,
          0,
          eventMessageService.DEFAULT_COUNT_MESSAGE,
          eventMessageService.MAX_COUNT_MESSAGE + getRandomInt(1, 100),
          -getRandomInt(1, eventMessageService.MAX_COUNT_MESSAGE * 10),
        ],
      ];

      const differentCountsTypesResults = [
        differentCountsTypes[0][0],
        eventMessageService.MAX_COUNT_MESSAGE,
        eventMessageService.DEFAULT_COUNT_MESSAGE,
      ];

      for (let i = 0; i < differentCountsTypes.length; i++) {
        const differentCounts = differentCountsTypes[i];
        const expectedCount = differentCountsTypesResults[i];

        for (let j = 0; j < differentCounts.length; j++) {
          const count = differentCounts[j];

          client1.send({
            actionId: 'eventMessageGet',
            path: ApiPath.EVENT_MESSAGE,
            method: RequestMethod.GET,
            body: {
              eventId: event1.getId(),
              count: count,
            },
          });

          client1.waitAnswers(1);
          const answers = (await client1.getAnswers())[0];

          expect(answers).to.deep.include({
            actionId: 'eventMessageGet',
            status: 200,
          });
          expect(answers.body.messages.length).to.equal(expectedCount);
          expect(answers.body.pointer).to.equal(
            countMessage - expectedCount - 1
          );

          client1.clear();
        }
      }
    });

    it('with different points', async () => {
      const correctValue = getRandomInt(1, countMessage);
      const differentPointersTypes = [
        [0],
        [correctValue],
        [
          undefined,
          -getRandomInt(1, 10000),
          getRandomInt(countMessage + 1, 10000),
        ],
      ];

      const nextPointer =
        correctValue - eventMessageService.DEFAULT_COUNT_MESSAGE;
      const differentPointersTypesResults = [
        0,
        nextPointer < 0 ? 0 : nextPointer,
        countMessage - eventMessageService.DEFAULT_COUNT_MESSAGE - 1,
      ];

      for (let i = 0; i < differentPointersTypes.length; i++) {
        const differentPointers = differentPointersTypes[i];

        const expectedNextPointer = differentPointersTypesResults[i];

        for (let j = 0; j < differentPointers.length; j++) {
          const pointer = differentPointers[j];

          client1.send({
            actionId: 'eventMessageGet',
            path: ApiPath.EVENT_MESSAGE,
            method: RequestMethod.GET,
            body: {
              eventId: event1.getId(),
              pointer,
            },
          });

          client1.waitAnswers(1);
          const answers = (await client1.getAnswers())[0];

          expect(answers).to.deep.include({
            actionId: 'eventMessageGet',
            status: 200,
          });
          expect(answers.body.messages.length).to.equal(
            expectedNextPointer > 0
              ? eventMessageService.DEFAULT_COUNT_MESSAGE
              : pointer + 1
          );
          expect(answers.body.pointer).to.equal(expectedNextPointer);
          client1.clear();
        }
      }
    });

    it('all by response data', async () => {
      let pointer = undefined;
      const allMessages = [];
      let i = 0;

      do {
        client1.send({
          actionId: 'eventMessageGet',
          path: ApiPath.EVENT_MESSAGE,
          method: RequestMethod.GET,
          body: {
            eventId: event1.getId(),
            pointer,
          },
        });

        client1.waitAnswers(i + 1);
        const answers = (await client1.getAnswers())[i];

        expect(answers).to.deep.include({
          actionId: 'eventMessageGet',
          status: 200,
        });

        allMessages.push(...answers.body.messages);
        pointer = answers.body.pointer;
        i++;
      } while (pointer !== 0);

      expect(allMessages.length).to.equal(countMessage);
    });

    it('from non-existent event', async () => {
      client1.send({
        actionId: 'eventMessageGet',
        path: ApiPath.EVENT_MESSAGE,
        method: RequestMethod.GET,
        body: {
          eventId: event2.getId(),
        },
      });

      client1.waitAnswers(1);
      const answers = (await client1.getAnswers())[0];

      expect(answers).to.deep.include({
        actionId: 'eventMessageGet',
        status: 404,
      });
      expect(answers.body.errorMessage).to.be.an('string');
    });
  });

  describe('delete', () => {
    it('correct', async () => {
      const message = await event1.createMessage(user1, 'привет');
      const message2 = await event1.createMessage(author, 'привет!');
      const willDeleteMessage = await event1.createMessage(user1, 'удали его');
      const message3 = await event1.createMessage(user1, 'последнее');

      client1.send({
        actionId: 'eventMessageDelete',
        path: ApiPath.EVENT_MESSAGE,
        method: RequestMethod.DELETE,
        body: {
          eventId: event1.getId(),
          messageId: String(willDeleteMessage._id),
        },
      });

      client1.waitAnswers(1);
      const answers = (await client1.getAnswers())[0];
      expect(answers).to.deep.include({
        actionId: 'eventMessageDelete',
        status: 200,
      });

      const event = await eventService.getById(event1.getId());

      expect(event.messages).to.include.members([
        String(message._id),
        String(message2._id),
        String(message3._id),
      ]);

      expect(event.messages).to.not.include(String(willDeleteMessage._id));

      const messageInDb = await messageService.getByIds([
        String(message._id),
        String(message2._id),
        String(message3._id),
      ]);

      expect(messageInDb.length).to.equal(3);

      const deletedMessageInDb = await messageService.getById(
        String(willDeleteMessage._id)
      );
      expect(deletedMessageInDb).to.equal(null);

      authorClient.waitRequests(1);
      let messageFomServer = (await authorClient.getRequests())[0];

      expect(String(willDeleteMessage._id)).to.deep.include(
        messageFomServer.body.messageId
      );
      expect(event1.getId()).to.deep.include(messageFomServer.body.eventId);

      client5.waitRequests(1);
      messageFomServer = (await client5.getRequests())[0];

      expect(String(willDeleteMessage._id)).to.deep.include(
        messageFomServer.body.messageId
      );
      expect(event1.getId()).to.deep.include(messageFomServer.body.eventId);
    });
    it('non-existent', async () => {
      const message = await event1.createMessage(user1, 'привет');

      client1.send({
        actionId: 'eventMessageDelete',
        path: ApiPath.EVENT_MESSAGE,
        method: RequestMethod.DELETE,
        body: {
          eventId: event2.getId(),
          messageId: String(message._id),
        },
      });

      client1.waitAnswers(1);
      const answers = (await client1.getAnswers())[0];

      expect(answers).to.deep.include({
        actionId: 'eventMessageDelete',
        status: 404,
      });
      expect(answers.body.errorMessage).to.be.an('string');

      authorClient.waitRequests(1);
      let messageFomServer = (await authorClient.getRequests())[0];

      expect(messageFomServer).to.equal(undefined);

      client5.waitRequests(1);
      messageFomServer = (await client5.getRequests())[0];

      expect(messageFomServer).to.equal(undefined);
    });
    it('another author', async () => {
      const message = await event1.createMessage(author, 'привет');

      client1.send({
        actionId: 'eventMessageDelete',
        path: ApiPath.EVENT_MESSAGE,
        method: RequestMethod.DELETE,
        body: {
          eventId: event1.getId(),
          messageId: String(message._id),
        },
      });

      client1.waitAnswers(1);
      const answers = (await client1.getAnswers())[0];

      expect(answers).to.deep.include({
        actionId: 'eventMessageDelete',
        status: 403,
      });
      expect(answers.body.errorMessage).to.be.an('string');

      authorClient.waitRequests(1);
      let messageFomServer = (await authorClient.getRequests())[0];

      expect(messageFomServer).to.equal(undefined);

      client5.waitRequests(1);
      messageFomServer = (await client5.getRequests())[0];

      expect(messageFomServer).to.equal(undefined);
    });
    it('another event', async () => {
      const event3 = new EventFixture(3);
      const user3 = new UserFixture(3);
      await user3.save();
      await event3.save(user1, category);

      const message = await event3.createMessage(user1, 'привет');

      client1.send({
        actionId: 'eventMessageDelete',
        path: ApiPath.EVENT_MESSAGE,
        method: RequestMethod.DELETE,
        body: {
          eventId: event1.getId(),
          messageId: String(message._id),
        },
      });

      client1.waitAnswers(1);
      const answers = (await client1.getAnswers())[0];

      expect(answers).to.deep.include({
        actionId: 'eventMessageDelete',
        status: 400,
      });
      expect(answers.body.errorMessage).to.be.an('string');

      authorClient.waitRequests(1);
      let messageFomServer = (await authorClient.getRequests())[0];

      expect(messageFomServer).to.equal(undefined);

      client5.waitRequests(1);
      messageFomServer = (await client5.getRequests())[0];

      expect(messageFomServer).to.equal(undefined);
    });
  });
});
