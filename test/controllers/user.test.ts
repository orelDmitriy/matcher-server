import 'mocha';
import { expect } from 'chai';
import request from 'supertest';
import mongoose from 'mongoose';
import { application } from '../../server';
import { GlobalModels } from '../../server/namespaces/GlobalModels';
import UserRole = GlobalModels.UserRole;
import UserState = GlobalModels.UserState;
import { UserFixture } from '../../server/testing/UserFixture';

describe('user controller', () => {
  const createdUser = new UserFixture(1);
  const newUser = new UserFixture(2);
  const admin = new UserFixture(4, true);

  beforeEach(async function () {
    await mongoose.connection.dropDatabase();
    await admin.save();
    await createdUser.save();
  });

  it('create user', () => {
    return request(application)
      .post('/api/v1/user')
      .send(newUser.getDefaultData())
      .expect('Content-Type', /json/)
      .then(response => {
        expect(response.status).to.equals(201);
        expect(response.body.user._id).to.be.an('string');
      });
  });

  it("can't create admin", () => {
    return request(application)
      .post('/api/v1/user')
      .send({
        ...newUser.getDefaultData(),
        role: UserRole.admin,
      })
      .expect('Content-Type', /json/)
      .then(response => {
        expect(response.status).to.equals(201);
        expect(response.body.user.role).to.be.equal(UserRole.user);
      });
  });

  it('create user without token', () => {
    return request(application)
      .post('/api/v1/user')
      .send({
        ...newUser.getDefaultData(),
        token: undefined,
      })
      .expect('Content-Type', /json/)
      .then(response => {
        expect(response.status).to.equals(400);
        expect(response.body.errors[0].fieldName).to.be.equal('token');
      });
  });

  it('create user with no unique nickname', async () => {
    return request(application)
      .post('/api/v1/user')
      .send({
        ...newUser.getDefaultData(),
        nickname: createdUser.getField('nickname'),
      })
      .expect('Content-Type', /json/)
      .then(response => {
        expect(response.status).to.equals(400);
        expect(response.body.errors[0].fieldName).to.be.equal('nickname');
      });
  });

  it('get user by nickname', async () => {
    return request(application)
      .get(`/api/v1/user/${createdUser.getField('nickname')}`)
      .expect('Content-Type', /json/)
      .then(response => {
        expect(response.status).to.equals(200);
        expect(response.body.user._id).to.be.an('string');
        expect(response.body.user.events).to.equal(undefined);
        expect(response.body.user.dialogs).to.equal(undefined);
      });
  });

  it('get full info by nickname', async () => {
    return request(application)
      .get(`/api/v1/user/${createdUser.getField('nickname')}`)
      .set(createdUser.getAuthData())
      .expect('Content-Type', /json/)
      .then(response => {
        expect(response.status).to.equals(200);
        expect(response.body.user._id).to.be.an('string');
        expect(response.body.user.events).to.be.an('array');
        expect(response.body.user.dialogs).to.be.an('array');
      });
  });

  it('get user by nonexistent nickname', () => {
    return request(application)
      .get(`/api/v1/user/${newUser.getField('nickname')}`)
      .expect('Content-Type', /json/)
      .then(response => {
        expect(response.status).to.equals(404);
        expect(response.body.errorMessage).to.be.an('string');
      });
  });

  it('update user', () => {
    return request(application)
      .patch(`/api/v1/user/${createdUser.getField('nickname')}`)
      .set(createdUser.getAuthData())
      .send({
        city: 'Polock',
      })
      .expect('Content-Type', /json/)
      .then(async response => {
        expect(response.status).to.equals(200);

        const userInDb = await createdUser.getDataFromDB();
        expect(userInDb.city).to.be.equal('Polock');
        expect(response.body.user.city).to.be.equal('Polock');
      });
  });

  it('block user', () => {
    return request(application)
      .put(`/api/v1/user/${createdUser.getField('nickname')}`)
      .set(admin.getAuthData())
      .send({
        state: UserState.blocked,
      })
      .expect('Content-Type', /json/)
      .then(response => {
        expect(response.status).to.equals(200);
        expect(response.body.user.state).to.be.equal(UserState.blocked);
      });
  });

  it('block blocked user', async () => {
    for (let i = 0; i < 2; i++) {
      await request(application)
        .put(`/api/v1/user/${createdUser.getField('nickname')}`)
        .set(admin.getAuthData())
        .send({
          state: UserState.blocked,
        })
        .expect('Content-Type', /json/)
        .then(response => {
          expect(response.status).to.equals(200);
          expect(response.body.user.state).to.be.equal(UserState.blocked);
        });
    }

    return request(application)
      .get(`/api/v1/user/${createdUser.getField('nickname')}`)
      .expect('Content-Type', /json/)
      .then(response => {
        expect(response.status).to.equals(200);
        expect(response.body.user.state).to.be.equal(UserState.blocked);
      });
  });

  it('restore user', async () => {
    for (let i = 0; i < 2; i++) {
      await request(application)
        .put(`/api/v1/user/${createdUser.getField('nickname')}`)
        .set(admin.getAuthData())
        .send({
          city: 'Lol',
          state: UserState.active,
        })
        .expect('Content-Type', /json/)
        .then(response => {
          expect(response.status).to.equals(200);
          expect(response.body.user.state).to.be.equal(UserState.active);
        });
    }
  });

  it('update user with wrong data', () => {
    return request(application)
      .patch(`/api/v1/user/${createdUser.getField('nickname')}`)
      .set(createdUser.getAuthData())
      .send({
        gender: '',
      })
      .expect('Content-Type', /json/)
      .then(response => {
        expect(response.status).to.equals(400);
        expect(response.body.errors[0].fieldName).to.be.equal('gender');
      });
  });

  it('update users readonly field', () => {
    return request(application)
      .patch(`/api/v1/user/${createdUser.getField('nickname')}`)
      .set(createdUser.getAuthData())
      .send({
        phone: 111111333,
        city: 'Some city',
      })
      .expect('Content-Type', /json/)
      .then(response => {
        expect(response.status).to.equals(200);
        expect(response.body.user.phone).to.be.equal(111111111);
        expect(response.body.user.city).to.be.equal('Some city');
      });
  });

  it('update user with wrong token', () => {
    return request(application)
      .patch(`/api/v1/user/${createdUser.getField('nickname')}`)
      .set({
        'auth-requester-token': 'token1000',
        'auth-requester-id': createdUser.getField('_id'),
      })
      .send({
        city: 'new city',
      })
      .expect('Content-Type', /json/)
      .then(response => {
        expect(response.status).to.equals(401);
        expect(response.body.errorMessage).to.be.an('string');
      });
  });

  it('update nonexistent user', () => {
    return request(application)
      .patch(`/api/v1/user/${newUser.getField('nickname')}`)
      .set(admin.getAuthData())
      .send({
        city: 'new city',
      })
      .expect('Content-Type', /json/)
      .then(response => {
        expect(response.status).to.equals(404);
        expect(response.body.errorMessage).to.be.an('string');
      });
  });

  it('delete nonexistent user', () => {
    return request(application)
      .delete(`/api/v1/user/${newUser.getField('nickname')}`)
      .set(admin.getAuthData())
      .expect('Content-Type', /json/)
      .then(response => {
        expect(response.status).to.equals(404);
        expect(response.body.errorMessage).to.be.an('string');
      });
  });

  it('delete user with wrong token', () => {
    return request(application)
      .delete(`/api/v1/user/${createdUser.getField('nickname')}`)
      .set({
        'auth-requester-token': 'token1000',
        'auth-requester-id': admin.getField('_id'),
      })
      .expect('Content-Type', /json/)
      .then(response => {
        expect(response.status).to.equals(401);
        expect(response.body.errorMessage).to.be.an('string');
      });
  });

  it('delete exist user', () => {
    return request(application)
      .delete(`/api/v1/user/${createdUser.getField('nickname')}`)
      .set(createdUser.getAuthData())
      .expect('Content-Type', /json/)
      .then(response => {
        expect(response.status).to.equals(200);
        expect(response.body.user.state).to.be.equal(UserState.deleted);
      });
  });

  it('delete deleted user', async () => {
    const app = request(application);
    await app
      .delete(`/api/v1/user/${createdUser.getField('nickname')}`)
      .set(createdUser.getAuthData());

    return app
      .delete(`/api/v1/user/${createdUser.getField('nickname')}`)
      .set(createdUser.getAuthData())
      .expect('Content-Type', /json/)
      .then(response => {
        expect(response.status).to.equals(401);
      });
  });

  it('get deleted user', async () => {
    const app = request(application);
    await app
      .delete(`/api/v1/user/${createdUser.getField('nickname')}`)
      .set(createdUser.getAuthData());

    return app
      .get(`/api/v1/user/${createdUser.getField('nickname')}`)
      .expect('Content-Type', /json/)
      .then(response => {
        expect(response.status).to.equals(200);
        expect(response.body.user.state).to.be.equal(UserState.deleted);
      });
  });

  it('delete user from admin', async () => {
    const app = request(application);
    await app
      .delete(`/api/v1/user/${createdUser.getField('nickname')}`)
      .set(createdUser.getAuthData());

    return request(application)
      .delete(`/api/v1/user/${createdUser.getField('nickname')}`)
      .set(admin.getAuthData())
      .expect('Content-Type', /json/)
      .then(response => {
        expect(response.status).to.equals(200);
        expect(response.body.user.state).to.be.equal(UserState.deleted);
      });
  });
});
