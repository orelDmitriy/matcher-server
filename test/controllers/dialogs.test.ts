import 'mocha';
import { expect } from 'chai';
import request from 'supertest';
import mongoose from 'mongoose';
import { application } from '../../server';
import { UserFixture } from '../../server/testing/UserFixture';
import { DialogFixture } from '../../server/testing/DialogFixture';
import { ApiPath } from '../../server/constants/ApiPath';

describe('dialogs controller', () => {
  const user1 = new UserFixture(1);
  const user2 = new UserFixture(2);
  const user3 = new UserFixture(3);
  const dialog1 = new DialogFixture(1);
  const dialog2 = new DialogFixture(2);

  beforeEach(async function () {
    await mongoose.connection.dropDatabase();
    await user1.save();
    await user2.save();
    await user3.save();
    await dialog1.save(user1, user2);
    await dialog2.save(user1, user3);
  });

  it('get user dialogs', async () => {
    return request(application)
      .get(ApiPath.DIALOGS)
      .set(user1.getAuthData())
      .then(async response => {
        expect(response.status).to.equals(200);
        expect(response.body.dialogs.length).to.equal(2);
      });
  });
});
