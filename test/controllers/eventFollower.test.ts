import 'mocha';
import { expect } from 'chai';
import request from 'supertest';
import mongoose from 'mongoose';

import { application } from '../../server';
import { eventService } from '../../server/servicies/eventService';
import { UserFixture } from '../../server/testing/UserFixture';
import { CategoryFixture } from '../../server/testing/CategoryFixture';
import { EventFixture } from '../../server/testing/EventFixture';
import { MongoModels } from '../../server/namespaces/MongoModels';
import { getRandomInt } from '../../server/utils/utilites';
import IEvent = MongoModels.IEvent;
import { eventsFollowerService } from '../../server/servicies/eventsFollowerService';

describe('events follower controller', function () {
  this.timeout(6000);
  const creator = new UserFixture(1);
  const existUser = new UserFixture(2);
  const nonexistentUser = new UserFixture(3);
  const follower = new UserFixture(4);
  const member = new UserFixture(5);
  const blocked = new UserFixture(6);
  const category = new CategoryFixture(1);
  const event = new EventFixture(1);
  const closedEvent = new EventFixture(2);
  const nonexistentEvent = new EventFixture(2);

  beforeEach(async () => {
    await mongoose.connection.dropDatabase();
    await category.save();
    await creator.save();
    await existUser.save();
    await event.save(creator, category);
    await closedEvent.save(creator, category);
    await follower.save();
    await member.save();
    await blocked.save();

    const events = [event, closedEvent];
    for (let i = 0; i < events.length; i++) {
      await events[i].addFollower(follower);
      await events[i].addMember(member);
      await events[i].addBlocked(blocked);

      const eventInDB: IEvent = await eventService.getFullDataById(
        events[i].getId()
      );

      expect(eventInDB.followers.length).to.equal(1);
      expect(eventInDB.followers[0]).to.equal(follower.getId());
      expect(eventInDB.members.length).to.equal(1);
      expect(eventInDB.members[0]).to.equal(member.getId());
      expect(eventInDB.usersBlocked.length).to.equal(1);
      expect(eventInDB.usersBlocked[0]).to.equal(blocked.getId());
    }

    await closedEvent.close();
  });

  describe('add to followers', () => {
    it('add user', () => {
      return request(application)
        .post(`/api/v1/event/${event.getId()}/follower`)
        .set({
          'auth-requester-token': existUser.getField('token'),
          'auth-requester-id': existUser.getId(),
        })
        .then(async response => {
          expect(response.status).to.equals(204);
          expect(response.noContent).to.equal(true);

          const eventInDB: IEvent = await eventService.getFullDataById(
            event.getId()
          );

          expect(eventInDB.followers.length).to.equal(2);
          expect(eventInDB.followers[1]).to.equal(existUser.getId());
        });
    });
    it(`add random count followers`, async () => {
      const count = getRandomInt(3, 7);

      for (let i = 0; i < count; i++) {
        const user = new UserFixture(10 + i);
        await user.save();
        await request(application)
          .post(`/api/v1/event/${event.getId()}/follower`)
          .set({
            'auth-requester-token': user.getField('token'),
            'auth-requester-id': user.getId(),
          });
      }

      const eventInDB: IEvent = await eventService.getFullDataById(
        event.getId()
      );

      expect(eventInDB.followers.length).to.equal(count + 1);
    });
    it('add blocked or follower or member or creator', async () => {
      const users = [blocked, follower, member, creator];
      for (let i = 0; i < users.length; i++) {
        await request(application)
          .post(`/api/v1/event/${event.getId()}/follower`)
          .set({
            'auth-requester-token': users[i].getField('token'),
            'auth-requester-id': users[i].getId(),
          })
          .then(async response => {
            expect(response.status).to.equals(403);
            expect(response.body.errorMessage).to.be.an('string');

            const eventInDB: IEvent = await eventService.getFullDataById(
              event.getId()
            );

            expect(eventInDB.followers.length).to.equal(1);
          });
      }
    });
    it('add user to nonexistent event', async () => {
      return request(application)
        .post(`/api/v1/event/${nonexistentEvent.getId()}/follower`)
        .set({
          'auth-requester-token': existUser.getField('token'),
          'auth-requester-id': existUser.getId(),
        })
        .then(response => {
          expect(response.status).to.equals(404);
          expect(response.body.errorMessage).to.be.an('string');
        });
    });
    it('add user to closed event', async () => {
      return request(application)
        .post(`/api/v1/event/${closedEvent.getId()}/follower`)
        .set({
          'auth-requester-token': existUser.getField('token'),
          'auth-requester-id': existUser.getId(),
        })
        .then(response => {
          expect(response.status).to.equals(403);
          expect(response.body.errorMessage).to.be.an('string');
        });
    });
  });

  describe('remove from followers', () => {
    it('remove follower', () => {
      return request(application)
        .delete(`/api/v1/event/${event.getId()}/follower`)
        .set({
          'auth-requester-token': follower.getField('token'),
          'auth-requester-id': follower.getId(),
        })
        .then(async response => {
          expect(response.status).to.equals(204);
          expect(response.noContent).to.equal(true);

          const eventInDB: IEvent = await eventService.getFullDataById(
            event.getId()
          );

          expect(eventInDB.followers.length).to.equal(0);
        });
    });
    it('remove not follower', async () => {
      const users = [blocked, creator, member, existUser];
      for (let i = 0; i < users.length; i++) {
        await request(application)
          .delete(`/api/v1/event/${event.getId()}/follower`)
          .set({
            'auth-requester-token': users[i].getField('token'),
            'auth-requester-id': users[i].getId(),
          })
          .then(async response => {
            expect(response.status).to.equals(400);
            expect(response.body.errorMessage).to.be.an('string');

            const eventInDB: IEvent = await eventService.getFullDataById(
              event.getId()
            );

            expect(eventInDB.followers.length).to.equal(1);
          });
      }
    });
    it('remove follower form closed event', async () => {
      return request(application)
        .delete(`/api/v1/event/${closedEvent.getId()}/follower`)
        .set({
          'auth-requester-token': follower.getField('token'),
          'auth-requester-id': follower.getId(),
        })
        .then(response => {
          expect(response.status).to.equals(403);
          expect(response.body.errorMessage).to.be.an('string');
        });
    });
  });

  describe(`add to members`, () => {
    it('add follower', () => {
      return request(application)
        .post(`/api/v1/event/${event.getId()}/member`)
        .set({
          'auth-requester-token': creator.getField('token'),
          'auth-requester-id': creator.getId(),
        })
        .send({
          nickname: follower.getField('nickname'),
        })
        .then(async response => {
          expect(response.status).to.equals(204);
          expect(response.noContent).to.equal(true);

          const eventInDB: IEvent = await eventService.getFullDataById(
            event.getId()
          );

          expect(eventInDB.members.length).to.equal(2);
          expect(eventInDB.members).to.includes(follower.getId());
        });
    });
    it('add random count follower', async () => {
      const count = getRandomInt(3, 7);
      const users: UserFixture[] = [];
      for (let i = 0; i < count; i++) {
        const user = new UserFixture(10 + i);
        await user.save();

        await eventsFollowerService.addFollower(event.getId(), user.getId());
        users.push(user);
      }

      let eventInDB: IEvent = await eventService.getFullDataById(event.getId());
      expect(eventInDB.followers.length).to.equal(count + 1);

      for (let i = 0; i < count; i++) {
        await request(application)
          .post(`/api/v1/event/${event.getId()}/member`)
          .set({
            'auth-requester-token': creator.getField('token'),
            'auth-requester-id': creator.getId(),
          })
          .send({
            nickname: users[i].getField('nickname'),
          })
          .then(response => {
            expect(response.status).to.equals(204);
            expect(response.noContent).to.equal(true);
          });
      }

      eventInDB = await eventService.getFullDataById(event.getId());
      expect(eventInDB.followers.length).to.equal(1);
      expect(eventInDB.members.length).to.equal(count + 1);
    });
    it('add not follower to members', async () => {
      const users = [creator, nonexistentUser, existUser, blocked, member];
      for (let i = 0; i < users.length; i++) {
        await request(application)
          .post(`/api/v1/event/${event.getId()}/member`)
          .set({
            'auth-requester-token': creator.getField('token'),
            'auth-requester-id': creator.getId(),
          })
          .send({
            nickname: users[i].getField('nickname'),
          })
          .expect('Content-Type', /json/)
          .then(response => {
            expect(response.status).to.equals(400);
            expect(response.body.errorMessage).to.be.an('string');
          });
      }
    });
    it('not creator add follower', () => {
      const users = [existUser, blocked, member, follower];
      for (let i = 0; i < users.length; i++) {
        return request(application)
          .post(`/api/v1/event/${event.getId()}/member`)
          .set({
            'auth-requester-token': users[i].getField('token'),
            'auth-requester-id': users[i].getId(),
          })
          .send({
            nickname: follower.getField('nickname'),
          })
          .expect('Content-Type', /json/)
          .then(response => {
            expect(response.status).to.equals(403);
            expect(response.body.errorMessage).to.be.an('string');
          });
      }
    });
    it('add follower in closed event', async () => {
      return request(application)
        .post(`/api/v1/event/${closedEvent.getId()}/member`)
        .set({
          'auth-requester-token': follower.getField('token'),
          'auth-requester-id': follower.getId(),
        })
        .expect('Content-Type', /json/)
        .then(response => {
          expect(response.status).to.equals(403);
          expect(response.body.errorMessage).to.be.an('string');
        });
    });
    it('add more then event max person', async () => {
      await eventService.update(
        event.getId(),
        {
          maxPerson: 1,
        },
        creator.getId()
      );
      const secondFollower = new UserFixture(8);
      await secondFollower.save();
      await eventsFollowerService.addFollower(
        event.getId(),
        secondFollower.getId()
      );

      return request(application)
        .post(`/api/v1/event/${event.getId()}/member`)
        .set({
          'auth-requester-token': creator.getField('token'),
          'auth-requester-id': creator.getId(),
        })
        .send({
          nickname: secondFollower.getField('nickname'),
        })
        .expect('Content-Type', /json/)
        .then(response => {
          expect(response.status).to.equals(403);
          expect(response.body.errorMessage).to.be.an('string');
        });
    });
  });

  describe(`remove from members`, () => {
    it('remove member', () => {
      return request(application)
        .delete(`/api/v1/event/${event.getId()}/member`)
        .set({
          'auth-requester-token': creator.getField('token'),
          'auth-requester-id': creator.getId(),
        })
        .send({
          nickname: member.getField('nickname'),
        })
        .then(async response => {
          expect(response.status).to.equals(204);
          expect(response.noContent).to.equal(true);

          const eventInDB: IEvent = await eventService.getFullDataById(
            event.getId()
          );

          expect(eventInDB.members.length).to.equal(0);
          expect(eventInDB.followers.length).to.equal(2);
          expect(eventInDB.followers).to.includes(member.getId());
        });
    });
    it('remove not member', async () => {
      const users = [creator, nonexistentUser, existUser, follower, blocked];
      for (let i = 0; i < users.length; i++) {
        await request(application)
          .delete(`/api/v1/event/${event.getId()}/member`)
          .set({
            'auth-requester-token': creator.getField('token'),
            'auth-requester-id': creator.getId(),
          })
          .send({
            nickname: users[i].getField('nickname'),
          })
          .expect('Content-Type', /json/)
          .then(async response => {
            expect(response.status).to.equals(400);
            expect(response.body.errorMessage).to.be.an('string');

            const eventInDB: IEvent = await eventService.getFullDataById(
              event.getId()
            );

            expect(eventInDB.members.length).to.equal(1);
          });
      }
    });
    it('not creator remove member', async () => {
      const users = [existUser, follower, member, blocked];
      for (let i = 0; i < users.length; i++) {
        await request(application)
          .delete(`/api/v1/event/${event.getId()}/member`)
          .set({
            'auth-requester-token': users[i].getField('token'),
            'auth-requester-id': users[i].getId(),
          })
          .send({
            nickname: member.getField('nickname'),
          })
          .expect('Content-Type', /json/)
          .then(async response => {
            expect(response.status).to.equals(403);
            expect(response.body.errorMessage).to.be.an('string');

            const eventInDB: IEvent = await eventService.getFullDataById(
              event.getId()
            );

            expect(eventInDB.members.length).to.equal(1);
          });
      }
    });
    it('remove in closed event', async () => {
      return request(application)
        .delete(`/api/v1/event/${closedEvent.getId()}/member`)
        .set({
          'auth-requester-token': creator.getField('token'),
          'auth-requester-id': creator.getId(),
        })
        .send({
          nickname: member.getField('nickname'),
        })
        .then(response => {
          expect(response.status).to.equals(403);
          expect(response.body.errorMessage).to.be.an('string');
        });
    });
  });

  describe('add to blocked', () => {
    it('add member or follower', async () => {
      await request(application)
        .post(`/api/v1/event/${event.getId()}/blocked`)
        .set({
          'auth-requester-token': creator.getField('token'),
          'auth-requester-id': creator.getId(),
        })
        .send({
          nickname: follower.getField('nickname'),
        })
        .then(async response => {
          expect(response.status).to.equals(204);
          expect(response.noContent).to.equal(true);

          const eventInDB: IEvent = await eventService.getFullDataById(
            event.getId()
          );

          expect(eventInDB.followers.length).to.equal(0);
          expect(eventInDB.usersBlocked.length).to.equal(2);
        });

      await request(application)
        .post(`/api/v1/event/${event.getId()}/blocked`)
        .set({
          'auth-requester-token': creator.getField('token'),
          'auth-requester-id': creator.getId(),
        })
        .send({
          nickname: member.getField('nickname'),
        })
        .then(async response => {
          expect(response.status).to.equals(204);
          expect(response.noContent).to.equal(true);

          const eventInDB: IEvent = await eventService.getFullDataById(
            event.getId()
          );

          expect(eventInDB.members.length).to.equal(0);
          expect(eventInDB.usersBlocked.length).to.equal(3);
        });
    });
    it('add not member or follower', async () => {
      const users = [nonexistentUser, existUser, creator, blocked];
      for (let i = 0; i < users.length; i++) {
        await request(application)
          .post(`/api/v1/event/${event.getId()}/blocked`)
          .set({
            'auth-requester-token': creator.getField('token'),
            'auth-requester-id': creator.getId(),
          })
          .send({
            nickname: users[i].getField('nickname'),
          })
          .expect('Content-Type', /json/)
          .then(async response => {
            expect(response.status).to.equals(400);
            expect(response.body.errorMessage).to.be.an('string');

            const eventInDB: IEvent = await eventService.getFullDataById(
              event.getId()
            );

            expect(eventInDB.usersBlocked.length).to.equal(1);
          });
      }
    });
    it('not creator add to blocked', async () => {
      const users = [existUser, blocked, follower, member];
      for (let i = 0; i < users.length; i++) {
        await request(application)
          .post(`/api/v1/event/${event.getId()}/blocked`)
          .set({
            'auth-requester-token': users[i].getField('token'),
            'auth-requester-id': users[i].getId(),
          })
          .send({
            nickname: follower.getField('nickname'),
          })
          .expect('Content-Type', /json/)
          .then(async response => {
            expect(response.status).to.equals(403);
            expect(response.body.errorMessage).to.be.an('string');

            const eventInDB: IEvent = await eventService.getFullDataById(
              event.getId()
            );

            expect(eventInDB.usersBlocked.length).to.equal(1);
          });
      }
    });
    it('add in closed event', () => {
      return request(application)
        .post(`/api/v1/event/${closedEvent.getId()}/blocked`)
        .set({
          'auth-requester-token': creator.getField('token'),
          'auth-requester-id': creator.getId(),
        })
        .send({
          nickname: follower.getField('nickname'),
        })
        .expect('Content-Type', /json/)
        .then(async response => {
          expect(response.status).to.equals(403);

          const eventInDB: IEvent = await eventService.getFullDataById(
            event.getId()
          );

          expect(eventInDB.usersBlocked.length).to.equal(1);
        });
    });
  });

  describe('remove from blocked', () => {
    it('remove blocked', () => {
      return request(application)
        .delete(`/api/v1/event/${event.getId()}/blocked`)
        .set({
          'auth-requester-token': creator.getField('token'),
          'auth-requester-id': creator.getId(),
        })
        .send({
          nickname: blocked.getField('nickname'),
        })
        .then(async response => {
          expect(response.status).to.equals(204);
          expect(response.noContent).to.equal(true);

          const eventInDB: IEvent = await eventService.getFullDataById(
            event.getId()
          );

          expect(eventInDB.followers.length).to.equal(2);
          expect(eventInDB.usersBlocked.length).to.equal(0);
        });
    });
    it('remove not blocked', async () => {
      const users = [nonexistentUser, existUser, creator, follower, member];
      for (let i = 0; i < users.length; i++) {
        await request(application)
          .delete(`/api/v1/event/${event.getId()}/blocked`)
          .set({
            'auth-requester-token': creator.getField('token'),
            'auth-requester-id': creator.getId(),
          })
          .send({
            nickname: users[i].getField('nickname'),
          })
          .expect('Content-Type', /json/)
          .then(async response => {
            expect(response.status).to.equals(400);
            expect(response.body.errorMessage).to.be.an('string');

            const eventInDB: IEvent = await eventService.getFullDataById(
              event.getId()
            );

            expect(eventInDB.usersBlocked.length).to.equal(1);
          });
      }
    });
    it('not creator remove', async () => {
      const users = [existUser, blocked, follower, member];
      for (let i = 0; i < users.length; i++) {
        await request(application)
          .delete(`/api/v1/event/${event.getId()}/blocked`)
          .set({
            'auth-requester-token': users[i].getField('token'),
            'auth-requester-id': users[i].getId(),
          })
          .send({
            nickname: blocked.getField('nickname'),
          })
          .expect('Content-Type', /json/)
          .then(async response => {
            expect(response.status).to.equals(403);
            expect(response.body.errorMessage).to.be.an('string');

            const eventInDB: IEvent = await eventService.getFullDataById(
              event.getId()
            );

            expect(eventInDB.usersBlocked.length).to.equal(1);
          });
      }
    });
    it('remove in closed event', () => {
      return request(application)
        .delete(`/api/v1/event/${closedEvent.getId()}/blocked`)
        .set({
          'auth-requester-token': creator.getField('token'),
          'auth-requester-id': creator.getId(),
        })
        .send({
          nickname: blocked.getField('nickname'),
        })
        .expect('Content-Type', /json/)
        .then(async response => {
          expect(response.status).to.equals(403);

          const eventInDB: IEvent = await eventService.getFullDataById(
            closedEvent.getId()
          );

          expect(eventInDB.usersBlocked.length).to.equal(1);
        });
    });
  });
});
