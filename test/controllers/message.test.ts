import 'mocha';
import { expect } from 'chai';
import mongoose from 'mongoose';

import { WsClientFixture } from '../../server/testing/WsClientFixture';
import { UserFixture } from '../../server/testing/UserFixture';
import { DialogFixture } from '../../server/testing/DialogFixture';
import { WsModels } from '../../server/namespaces/WsModels';
import RequestMethod = WsModels.RequestMethod;
import { ApiPath } from '../../server/constants/ApiPath';

describe('message', function () {
  this.timeout(6000);

  const client1 = new WsClientFixture();
  const client2 = new WsClientFixture();
  const user1 = new UserFixture(1);
  const user2 = new UserFixture(2);
  const dialog1 = new DialogFixture(1);

  beforeEach(async () => {
    await mongoose.connection.dropDatabase();
    await user1.save();
    await user2.save();
    client1.clear();
    client2.clear();
    await client1.authorize(user1);
    await client2.authorize(user2);
    await dialog1.save(user1, user2);
  });

  describe('message update', () => {
    it('correct', async () => {
      const message = await dialog1.createMessage(user1, 'привет');
      const updateMessage = 'Привет!';
      client1.send({
        actionId: 'messageUpdate',
        path: ApiPath.MESSAGE,
        method: RequestMethod.PATCH,
        body: {
          dialogId: dialog1.getId(),
          messageId: String(message._id),
          message: updateMessage,
        },
      });

      client1.waitAnswers(1);
      const answers = (await client1.getAnswers())[0];

      expect(answers).to.deep.include({
        actionId: 'messageUpdate',
        status: 200,
      });
      expect(answers.body.message.message).to.equal(updateMessage);

      client2.waitRequests(1);
      let messageFomServer = (await client2.getRequests())[0];

      expect(answers.body.message).to.deep.include(
        messageFomServer.body.message
      );
      expect(dialog1.getId()).to.deep.include(messageFomServer.body.dialogId);

      client2.send({
        actionId: 'messageUpdate',
        path: ApiPath.MESSAGE,
        method: RequestMethod.PATCH,
        body: {
          dialogId: dialog1.getId(),
          messageId: String(message._id),
          read: true,
        },
      });

      client2.waitAnswers(1);
      const answers2 = (await client2.getAnswers())[0];

      expect(answers2).to.deep.include({
        actionId: 'messageUpdate',
        status: 200,
      });
      expect(answers2.body.message.read).to.includes(user2.getId());

      client1.waitRequests(1);
      messageFomServer = (await client1.getRequests())[0];

      expect(answers2.body.message).to.deep.include(
        messageFomServer.body.message
      );
      expect(dialog1.getId()).to.deep.include(messageFomServer.body.dialogId);
    });

    it('non-exist', async () => {
      const updateMessage = 'Привет!';
      client1.send({
        actionId: 'messageUpdate',
        path: ApiPath.MESSAGE,
        method: RequestMethod.PATCH,
        body: {
          dialogId: dialog1.getId(),
          messageId: dialog1.getId(),
          message: updateMessage,
        },
      });

      client1.waitAnswers(1);
      const answers = (await client1.getAnswers())[0];

      expect(answers).to.deep.include({
        actionId: 'messageUpdate',
        status: 404,
      });
      expect(answers.body.errorMessage).to.be.an('string');

      client2.waitRequests(1);
      const messageFomServer = (await client2.getRequests())[0];

      expect(messageFomServer).to.equal(undefined);
    });

    it('alien', async () => {
      const message = await dialog1.createMessage(user2, 'привет');
      const updateMessage = 'Привет!';
      client1.send({
        actionId: 'messageUpdate',
        path: ApiPath.MESSAGE,
        method: RequestMethod.PATCH,
        body: {
          dialogId: dialog1.getId(),
          messageId: String(message._id),
          message: updateMessage,
        },
      });

      client1.waitAnswers(1);
      const answers = (await client1.getAnswers())[0];

      expect(answers).to.deep.include({
        actionId: 'messageUpdate',
        status: 403,
      });
      expect(answers.body.errorMessage).to.be.an('string');

      client2.waitRequests(1);
      const messageFomServer = (await client2.getRequests())[0];

      expect(messageFomServer).to.equal(undefined);
    });

    it('without message or read field', async () => {
      const message = await dialog1.createMessage(user1, 'привет');
      client1.send({
        actionId: 'messageUpdate',
        path: ApiPath.MESSAGE,
        method: RequestMethod.PATCH,
        body: {
          dialogId: dialog1.getId(),
          messageId: String(message._id),
        },
      });

      client1.waitAnswers(1);
      const answers = (await client1.getAnswers())[0];

      expect(answers).to.deep.include({
        actionId: 'messageUpdate',
        status: 400,
      });
      expect(answers.body.errorMessage).to.be.an('string');

      client2.waitRequests(1);
      const messageFomServer = (await client2.getRequests())[0];

      expect(messageFomServer).to.equal(undefined);
    });

    it('with incorrect data', async () => {
      const message = await dialog1.createMessage(user1, 'привет');
      client1.send({
        actionId: 'messageUpdate',
        path: ApiPath.MESSAGE,
        method: RequestMethod.PATCH,
        body: {
          dialogId: dialog1.getId(),
          messageId: String(message._id),
          message: '',
        },
      });

      client1.waitAnswers(1);
      const answers = (await client1.getAnswers())[0];

      expect(answers).to.deep.include({
        actionId: 'messageUpdate',
        status: 400,
      });
      expect(answers.body.errorMessage).to.be.an('string');

      client2.waitRequests(1);
      const messageFomServer = (await client2.getRequests())[0];

      expect(messageFomServer).to.equal(undefined);
    });
  });
});
