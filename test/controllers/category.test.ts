import 'mocha';
import { expect } from 'chai';
import request from 'supertest';
import mongoose from 'mongoose';

import { application } from '../../server';
import { UserFixture } from '../../server/testing/UserFixture';
import { CategoryFixture } from '../../server/testing/CategoryFixture';

describe('category controller', () => {
  const admin = new UserFixture(1, true);
  const user = new UserFixture(2);
  const category = new CategoryFixture(1);

  beforeEach(async function () {
    await mongoose.connection.dropDatabase();
    await admin.save();
    await user.save();
  });

  it('get empty categories', () => {
    return request(application)
      .get('/api/v1/categories')
      .expect('Content-Type', /json/)
      .then(response => {
        expect(response.status).to.equals(200);
        expect(response.body.categories).to.be.an('array');
        expect(response.body.categories.length).to.be.equal(0);
      });
  });

  it('create category without admin role', () => {
    return request(application)
      .post('/api/v1/category')
      .set({
        'auth-requester-token': user.getField('token'),
        'auth-requester-id': user.getField('_id'),
      })
      .send({
        category: category.getField('category'),
      })
      .expect('Content-Type', /json/)
      .then(response => {
        expect(response.status).to.equals(401);
        expect(response.body.errorMessage).to.be.an('string');
      });
  });

  it('create category with admin role', () => {
    return request(application)
      .post('/api/v1/category')
      .set({
        'auth-requester-token': admin.getField('token'),
        'auth-requester-id': admin.getField('_id'),
      })
      .send({
        category: category.getField('category'),
      })
      .expect('Content-Type', /json/)
      .then(response => {
        expect(response.status).to.equals(201);
        //let category = response.body.category;
        expect(response.body.category).to.be.an('object');
      });
  });

  describe('with created category', () => {
    beforeEach(async function () {
      await category.save();
    });

    it('create category with existing name', () => {
      return request(application)
        .post('/api/v1/category')
        .set({
          'auth-requester-token': admin.getField('token'),
          'auth-requester-id': admin.getField('_id'),
        })
        .send({
          category: category.getField('category'),
        })
        .expect('Content-Type', /json/)
        .then(response => {
          expect(response.status).to.equals(400);
          expect(response.body.errors[0].fieldName).to.be.equal('category');
        });
    });

    it('delete category without author role', () => {
      return request(application)
        .delete(`/api/v1/category/${category.getField('_id')}`)
        .set({
          'auth-requester-token': user.getField('token'),
          'auth-requester-id': user.getField('_id'),
        })
        .expect('Content-Type', /json/)
        .then(response => {
          expect(response.status).to.equals(401);
          expect(response.body.errorMessage).to.be.an('string');

          return request(application)
            .get('/api/v1/categories')
            .expect('Content-Type', /json/)
            .then(response => {
              expect(response.status).to.equals(200);
              expect(response.body.categories[0]).to.nested.include({
                category: category.getField('category'),
              });
            });
        });
    });

    it('delete category', () => {
      return request(application)
        .delete(`/api/v1/category/${category.getField('_id')}`)
        .set({
          'auth-requester-token': admin.getField('token'),
          'auth-requester-id': admin.getField('_id'),
        })
        .expect('Content-Type', /json/)
        .then(response => {
          expect(response.status).to.equals(200);
        });
    });
  });

  it('delete nonexistent category', () => {
    return request(application)
      .delete(`/api/v1/category/${admin.getField('_id')}`)
      .set({
        'auth-requester-token': admin.getField('token'),
        'auth-requester-id': admin.getField('_id'),
      })
      .expect('Content-Type', /json/)
      .then(response => {
        expect(response.status).to.equals(200);
      });
  });
});
