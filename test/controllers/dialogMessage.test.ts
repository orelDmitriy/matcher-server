import 'mocha';
import { expect } from 'chai';
import mongoose from 'mongoose';

import { WsClientFixture } from '../../server/testing/WsClientFixture';
import { UserFixture } from '../../server/testing/UserFixture';
import { DialogFixture } from '../../server/testing/DialogFixture';
import { WsModels } from '../../server/namespaces/WsModels';
import RequestMethod = WsModels.RequestMethod;
import { ApiPath } from '../../server/constants/ApiPath';
import { dialogMessageService } from '../../server/servicies/dialogMessageService';
import { getRandomInt } from '../../server/utils/utilites';
import { messageService } from '../../server/servicies/messageService';
import { dialogService } from '../../server/servicies/dialogService';

describe('dialog message', function () {
  this.timeout(6000);

  const client1 = new WsClientFixture();
  const client2 = new WsClientFixture();
  const user1 = new UserFixture(1);
  const user2 = new UserFixture(2);
  const dialog1 = new DialogFixture(1);
  const dialog2 = new DialogFixture(2);

  beforeEach(async () => {
    await mongoose.connection.dropDatabase();
    await user1.save();
    await user2.save();
    client1.clear();
    client2.clear();
    await client1.authorize(user1);
    await client2.authorize(user2);
    await dialog1.save(user1, user2);
  });

  describe('dialog message create', () => {
    it('correct', async () => {
      client1.send({
        actionId: 'dialogMessageCreate',
        path: ApiPath.DIALOG_MESSAGE,
        method: RequestMethod.POST,
        body: {
          dialogId: dialog1.getId(),
          message: 'some message',
        },
      });

      client1.waitAnswers(1);
      const answers = (await client1.getAnswers())[0];

      expect(answers).to.deep.include({
        actionId: 'dialogMessageCreate',
        status: 201,
      });
      expect(answers.body.message._id).to.be.an('string');

      client2.waitRequests(1);
      const messageFomServer = (await client2.getRequests())[0];

      expect(answers.body.message).to.deep.include(
        messageFomServer.body.message
      );
      expect(dialog1.getId()).to.deep.include(messageFomServer.body.dialogId);
    });

    it('without required data', async () => {
      client1.send({
        actionId: 'dialogMessageCreate',
        path: ApiPath.DIALOG_MESSAGE,
        method: RequestMethod.POST,
        body: {
          dialogId: dialog1.getId(),
          message: '',
        },
      });

      client1.waitAnswers(1);
      const answers = (await client1.getAnswers())[0];

      expect(answers).to.deep.include({
        actionId: 'dialogMessageCreate',
        status: 400,
      });
      expect(answers.body.errorMessage).to.be.an('string');

      client2.waitRequests(1);
      const messageFomServer = (await client2.getRequests())[0];

      expect(messageFomServer).to.equal(undefined);
    });

    it('non-existent dialog', async () => {
      client1.send({
        actionId: 'dialogMessageCreate',
        path: ApiPath.DIALOG_MESSAGE,
        method: RequestMethod.POST,
        body: {
          dialogId: dialog2.getId(),
          message: 'some message',
        },
      });

      client1.waitAnswers(1);
      const answers = (await client1.getAnswers())[0];

      expect(answers).to.deep.include({
        actionId: 'dialogMessageCreate',
        status: 404,
      });
      expect(answers.body.errorMessage).to.be.an('string');

      client2.waitRequests(1);
      const messageFomServer = (await client2.getRequests())[0];

      expect(messageFomServer).to.equal(undefined);
    });
  });

  describe('dialog message get', () => {
    const countMessage = 130;

    beforeEach(async () => {
      const users = [user1, user2];
      const messages = [];
      for (let i = 0; i < countMessage; i++) {
        messages.push({
          creatorId: users[i % 2].getId(),
          message: `Message ${i}.`,
        });
      }

      await dialog1.addAllMessages(messages);
    });

    it('with different counts and result types', async () => {
      const differentCountsTypes = [
        [getRandomInt(1, dialogMessageService.MAX_COUNT_MESSAGE)],
        [dialogMessageService.MAX_COUNT_MESSAGE],
        [
          undefined,
          0,
          dialogMessageService.DEFAULT_COUNT_MESSAGE,
          dialogMessageService.MAX_COUNT_MESSAGE + getRandomInt(1, 100),
          -getRandomInt(1, dialogMessageService.MAX_COUNT_MESSAGE * 10),
        ],
      ];

      const differentCountsTypesResults = [
        differentCountsTypes[0][0],
        dialogMessageService.MAX_COUNT_MESSAGE,
        dialogMessageService.DEFAULT_COUNT_MESSAGE,
      ];

      for (let i = 0; i < differentCountsTypes.length; i++) {
        const differentCounts = differentCountsTypes[i];
        const expectedCount = differentCountsTypesResults[i];

        for (let j = 0; j < differentCounts.length; j++) {
          const count = differentCounts[j];

          client1.send({
            actionId: 'dialogMessageGet',
            path: ApiPath.DIALOG_MESSAGE,
            method: RequestMethod.GET,
            body: {
              dialogId: dialog1.getId(),
              count: count,
            },
          });

          client1.waitAnswers(1);
          const answers = (await client1.getAnswers())[0];

          expect(answers).to.deep.include({
            actionId: 'dialogMessageGet',
            status: 200,
          });
          expect(answers.body.messages.length).to.equal(expectedCount);
          expect(answers.body.pointer).to.equal(
            countMessage - expectedCount - 1
          );

          client1.clear();
        }
      }
    });

    it('with different points', async () => {
      const correctValue = getRandomInt(1, countMessage);
      const differentPointersTypes = [
        [0],
        [correctValue],
        [
          undefined,
          -getRandomInt(1, 10000),
          getRandomInt(countMessage + 1, 10000),
        ],
      ];

      const nextPointer =
        correctValue - dialogMessageService.DEFAULT_COUNT_MESSAGE;
      const differentPointersTypesResults = [
        0,
        nextPointer < 0 ? 0 : nextPointer,
        countMessage - dialogMessageService.DEFAULT_COUNT_MESSAGE - 1,
      ];

      for (let i = 0; i < differentPointersTypes.length; i++) {
        const differentPointers = differentPointersTypes[i];

        const expectedNextPointer = differentPointersTypesResults[i];

        for (let j = 0; j < differentPointers.length; j++) {
          const pointer = differentPointers[j];

          client1.send({
            actionId: 'dialogMessageGet',
            path: ApiPath.DIALOG_MESSAGE,
            method: RequestMethod.GET,
            body: {
              dialogId: dialog1.getId(),
              pointer,
            },
          });

          client1.waitAnswers(1);
          const answers = (await client1.getAnswers())[0];

          expect(answers).to.deep.include({
            actionId: 'dialogMessageGet',
            status: 200,
          });
          expect(answers.body.messages.length).to.equal(
            expectedNextPointer > 0
              ? dialogMessageService.DEFAULT_COUNT_MESSAGE
              : pointer + 1
          );
          expect(answers.body.pointer).to.equal(expectedNextPointer);
          client1.clear();
        }
      }
    });

    it('all by response data', async () => {
      let pointer = undefined;
      const allMessages = [];
      let i = 0;

      do {
        client1.send({
          actionId: 'dialogMessageGet',
          path: ApiPath.DIALOG_MESSAGE,
          method: RequestMethod.GET,
          body: {
            dialogId: dialog1.getId(),
            pointer,
          },
        });

        client1.waitAnswers(i + 1);
        const answers = (await client1.getAnswers())[i];

        expect(answers).to.deep.include({
          actionId: 'dialogMessageGet',
          status: 200,
        });

        allMessages.push(...answers.body.messages);
        pointer = answers.body.pointer;
        i++;
      } while (pointer !== 0);

      expect(allMessages.length).to.equal(countMessage);
    });

    it('from non-existent dialog', async () => {
      client1.send({
        actionId: 'dialogMessageGet',
        path: ApiPath.DIALOG_MESSAGE,
        method: RequestMethod.GET,
        body: {
          dialogId: dialog2.getId(),
        },
      });

      client1.waitAnswers(1);
      const answers = (await client1.getAnswers())[0];

      expect(answers).to.deep.include({
        actionId: 'dialogMessageGet',
        status: 404,
      });
      expect(answers.body.errorMessage).to.be.an('string');
    });
  });

  describe('dialog message delete', () => {
    it('correct', async () => {
      const message = await dialog1.createMessage(user1, 'привет');
      const message2 = await dialog1.createMessage(user2, 'привет!');
      const willDeleteMessage = await dialog1.createMessage(user1, 'удали его');
      const message3 = await dialog1.createMessage(user1, 'последнее');

      client1.send({
        actionId: 'dialogMessageDelete',
        path: ApiPath.DIALOG_MESSAGE,
        method: RequestMethod.DELETE,
        body: {
          dialogId: dialog1.getId(),
          messageId: String(willDeleteMessage._id),
        },
      });

      client1.waitAnswers(1);
      const answers = (await client1.getAnswers())[0];
      expect(answers).to.deep.include({
        actionId: 'dialogMessageDelete',
        status: 200,
      });

      const dialogInDb = await dialogService.getById(dialog1.getId());

      expect(dialogInDb.messages).to.include.members([
        String(message._id),
        String(message2._id),
        String(message3._id),
      ]);

      expect(dialogInDb.messages).to.not.include(String(willDeleteMessage._id));

      const messageInDb = await messageService.getByIds([
        String(message._id),
        String(message2._id),
        String(message3._id),
      ]);

      expect(messageInDb.length).to.equal(3);

      const deletedMessageInDb = await messageService.getById(
        String(willDeleteMessage._id)
      );
      expect(deletedMessageInDb).to.equal(null);

      client2.waitRequests(1);
      const messageFomServer = (await client2.getRequests())[0];

      expect(String(willDeleteMessage._id)).to.equal(
        messageFomServer.body.messageId
      );
      expect(dialog1.getId()).to.equal(messageFomServer.body.dialogId);
    });
    it('non-existent', async () => {
      const message = await dialog1.createMessage(user1, 'привет');

      client1.send({
        actionId: 'dialogMessageDelete',
        path: ApiPath.DIALOG_MESSAGE,
        method: RequestMethod.DELETE,
        body: {
          dialogId: dialog2.getId(),
          messageId: String(message._id),
        },
      });

      client1.waitAnswers(1);
      const answers = (await client1.getAnswers())[0];

      expect(answers).to.deep.include({
        actionId: 'dialogMessageDelete',
        status: 404,
      });
      expect(answers.body.errorMessage).to.be.an('string');

      client2.waitRequests(1);
      const messageFomServer = (await client2.getRequests())[0];

      expect(messageFomServer).to.equal(undefined);
    });
    it('another author', async () => {
      const message = await dialog1.createMessage(user2, 'привет');

      client1.send({
        actionId: 'dialogMessageDelete',
        path: ApiPath.DIALOG_MESSAGE,
        method: RequestMethod.DELETE,
        body: {
          dialogId: dialog1.getId(),
          messageId: String(message._id),
        },
      });

      client1.waitAnswers(1);
      const answers = (await client1.getAnswers())[0];

      expect(answers).to.deep.include({
        actionId: 'dialogMessageDelete',
        status: 403,
      });
      expect(answers.body.errorMessage).to.be.an('string');

      client2.waitRequests(1);
      const messageFomServer = (await client2.getRequests())[0];

      expect(messageFomServer).to.equal(undefined);
    });
    it('another dialog', async () => {
      const dialog3 = new DialogFixture(3);
      const user3 = new UserFixture(3);
      await user3.save();
      await dialog3.save(user1, user3);

      const message = await dialog3.createMessage(user1, 'привет');

      client1.send({
        actionId: 'dialogMessageDelete',
        path: ApiPath.DIALOG_MESSAGE,
        method: RequestMethod.DELETE,
        body: {
          dialogId: dialog1.getId(),
          messageId: String(message._id),
        },
      });

      client1.waitAnswers(1);
      const answers = (await client1.getAnswers())[0];

      expect(answers).to.deep.include({
        actionId: 'dialogMessageDelete',
        status: 400,
      });
      expect(answers.body.errorMessage).to.be.an('string');

      client2.waitRequests(1);
      const messageFomServer = (await client2.getRequests())[0];

      expect(messageFomServer).to.equal(undefined);
    });
  });
});
