import 'mocha';
import { expect } from 'chai';
import request from 'supertest';
import mongoose from 'mongoose';
import { application } from '../../server';
import { UserFixture } from '../../server/testing/UserFixture';
import { DialogFixture } from '../../server/testing/DialogFixture';
import { CategoryFixture } from '../../server/testing/CategoryFixture';
import { EventFixture } from '../../server/testing/EventFixture';

describe('users events and dialogs controller', () => {
  const userCount = 4;
  const users: Array<UserFixture> = new Array(userCount);
  const events: Array<Array<EventFixture>> = [[]];

  before(async function () {
    await mongoose.connection.dropDatabase();
    for (let i = 0; i < userCount; i++) {
      users[i] = new UserFixture(i);
      await users[i].save();
    }

    for (let i = 0; i < userCount; i++) {
      const dialog = new DialogFixture(i);
      await dialog.save(users[i], users[(i + 1) % userCount]);
    }

    const category1 = new CategoryFixture(1);
    await category1.save();
    const category2 = new CategoryFixture(2);
    await category2.save();

    for (let i = 0; i < userCount; i++) {
      events[i] = [];
      for (let j = i; j >= 0; j--) {
        events[i][j] = new EventFixture(i * 10 + j);
        await events[i][j].save(users[i], i % 2 === 0 ? category1 : category2);
      }
    }
  });

  it(`get events from nonexistent user`, () => {
    return request(application)
      .get(`/api/v1/user/someUserNameZxa/events`)
      .expect('Content-Type', /json/)
      .then(response => {
        expect(response.status).to.equals(404);
      });
  });

  for (let i = 0; i < users.length; i++) {
    it(`get users[${i}] all events`, () => {
      return request(application)
        .get(`/api/v1/user/${users[i].getField('nickname')}/events`)
        .set(users[i].getAuthData())
        .expect('Content-Type', /json/)
        .then(response => {
          expect(response.status).to.equals(200);
          expect(response.body.events).to.be.an('array');
          expect(response.body.events.length).to.equals(events[i]?.length || 0);
        });
    });
  }

  for (let i = 0; i < users.length; i++) {
    it(`get users[${i}] all dialogs`, () => {
      return request(application)
        .get(`/api/v1/user/${users[i].getField('nickname')}/dialogs`)
        .set({
          'auth-requester-token': users[i].getField('token'),
          'auth-requester-id': users[i].getField('_id'),
        })
        .expect('Content-Type', /json/)
        .then(response => {
          expect(response.status).to.equals(200);
          expect(response.body.dialogs).to.be.an('array');
          expect(response.body.dialogs.length).to.equals(2);
        });
    });
  }
});
