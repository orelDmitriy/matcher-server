import 'mocha';
import { expect } from 'chai';

import mongoose from 'mongoose';
import { WsClientFixture } from '../../server/testing/WsClientFixture';
import { WsModels } from '../../server/namespaces/WsModels';
import { UserFixture } from '../../server/testing/UserFixture';
import RequestMethod = WsModels.RequestMethod;
import { ApiPath } from '../../server/constants/ApiPath';

describe('authorization', () => {
  const client1 = new WsClientFixture();
  const client2 = new WsClientFixture();
  const user1 = new UserFixture(1);
  const user2 = new UserFixture(2);
  const blockedUser = new UserFixture(3);

  beforeEach(async () => {
    await mongoose.connection.dropDatabase();
    await user1.save();
    await user2.save();
    await blockedUser.save();
    await blockedUser.block();
    client1.clear();
  });

  it('correct', async () => {
    client1.send({
      actionId: 'correct',
      path: ApiPath.AUTHORIZATION,
      method: RequestMethod.POST,
      body: {
        userId: user1.getId(),
        token: user1.getField('token'),
      },
    });
    client1.waitAnswers(1);
    const answers = await client1.getAnswers();
    expect(answers[0]).to.deep.include({
      actionId: 'correct',
      status: 200,
    });
  });

  it('incorrect data', async () => {
    client1.send({
      actionId: 'incorrect data',
      path: ApiPath.AUTHORIZATION,
      method: RequestMethod.POST,
      body: {
        userId: user1.getId(),
        token: 'token',
      },
    });
    client1.waitAnswers(1);
    const answers = await client1.getAnswers();
    expect(answers[0].status).to.equal(403);
    expect(answers[0].body.errorMessage).to.be.an('string');
  });

  it('blocked user', async () => {
    const authData = {
      actionId: 'blocked user',
      path: ApiPath.AUTHORIZATION,
      method: RequestMethod.POST,
      body: {
        userId: blockedUser.getId(),
        token: blockedUser.getField('token'),
      },
    };

    client1.send(authData);
    client1.waitAnswers(1);
    const answers = await client1.getAnswers();
    expect(answers[0].status).to.equal(403);
    expect(answers[0].body.errorMessage).to.be.an('string');
  });

  it('send another request without authorization', async () => {
    client2.send({
      actionId: 'createDialog',
      path: ApiPath.DIALOG,
      method: RequestMethod.POST,
      body: {
        userId: user1.getId(),
      },
    });
    client2.waitAnswers(1);
    const answers = await client2.getAnswers();
    expect(answers[0].status).to.equal(401);
    expect(answers[0].body.errorMessage).to.be.an('string');
  });
});
