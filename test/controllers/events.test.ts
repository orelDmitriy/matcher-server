import 'mocha';
import { expect } from 'chai';
import request from 'supertest';
import mongoose from 'mongoose';
import { application } from '../../server';
import { UserFixture } from '../../server/testing/UserFixture';
import { CategoryFixture } from '../../server/testing/CategoryFixture';
import { EventFixture } from '../../server/testing/EventFixture';

describe('events controller', () => {
  const countEvents = 105;
  const defaultLimit = 10;
  const maxLimit = 100;
  const events: EventFixture[] = [];

  const admin = new UserFixture(1, true);
  const category1 = new CategoryFixture(1);

  before(async function () {
    await mongoose.connection.dropDatabase();
    await admin.save();
    await category1.save();
    for (let i = 1; i <= countEvents; i++) {
      const event = new EventFixture(i);
      await event.save(admin, category1);

      events.push(event);
    }
  });

  for (let i = 0; i + defaultLimit <= countEvents; i += defaultLimit) {
    it(`get events from ${i} limit ${defaultLimit}`, () => {
      return request(application)
        .get(`/api/v1/events?limit=${defaultLimit}&from=${i}`)
        .expect('Content-Type', /json/)
        .then(response => {
          expect(response.status).to.equals(200);
          expect(response.body.events).to.be.an('array');
          expect(response.body.events.length).to.be.equal(defaultLimit);
          expect(response.body.events[0]._id).to.be.equal(
            String(events[i].getField('_id'))
          );
        });
    });
  }

  it(`get events from -1`, () => {
    return request(application)
      .get(`/api/v1/events?from=-1`)
      .expect('Content-Type', /json/)
      .then(response => {
        expect(response.status).to.equals(200);
        expect(response.body.events).to.be.an('array');
        expect(response.body.events.length).to.be.equal(defaultLimit);
        expect(response.body.events[0]._id).to.be.equal(
          String(events[0].getField('_id'))
        );
      });
  });

  it(`get events from more then exist`, () => {
    return request(application)
      .get(`/api/v1/events?from=${countEvents + 1}`)
      .expect('Content-Type', /json/)
      .then(response => {
        expect(response.status).to.equals(200);
        expect(response.body.events).to.be.an('array');
        expect(response.body.events.length).to.be.equal(0);
      });
  });

  it(`get events limit less then 1`, () => {
    return request(application)
      .get(`/api/v1/events?limit=${0}`)
      .expect('Content-Type', /json/)
      .then(response => {
        expect(response.status).to.equals(200);
        expect(response.body.events).to.be.an('array');
        expect(response.body.events.length).to.be.equal(defaultLimit);
      });
  });

  it(`get events limit more then ${maxLimit}`, () => {
    return request(application)
      .get(`/api/v1/events?limit=${maxLimit + 1}`)
      .expect('Content-Type', /json/)
      .then(response => {
        expect(response.status).to.equals(200);
        expect(response.body.events).to.be.an('array');
        expect(response.body.events.length).to.be.equal(maxLimit);
      });
  });

  const countLastEvent = 3;
  it(`get events from end if not enough events`, () => {
    return request(application)
      .get(`/api/v1/events?from=${countEvents - countLastEvent}`)
      .expect('Content-Type', /json/)
      .then(response => {
        expect(response.status).to.equals(200);
        expect(response.body.events).to.be.an('array');
        expect(response.body.events.length).to.be.equal(countLastEvent);
      });
  });
});
