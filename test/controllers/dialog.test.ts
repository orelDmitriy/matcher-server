import 'mocha';
import { expect } from 'chai';
import mongoose from 'mongoose';

import { UserFixture } from '../../server/testing/UserFixture';
import { WsClientFixture } from '../../server/testing/WsClientFixture';
import { WsModels } from '../../server/namespaces/WsModels';
import { DialogFixture } from '../../server/testing/DialogFixture';
import { ApiPath } from '../../server/constants/ApiPath';
import { messageService } from '../../server/servicies/messageService';
import RequestMethod = WsModels.RequestMethod;

describe('dialog', function () {
  this.timeout(6000);
  const client1 = new WsClientFixture();
  const client2 = new WsClientFixture();
  const client4 = new WsClientFixture();
  const clientBlocked = new WsClientFixture();
  const user1 = new UserFixture(1);
  const user2 = new UserFixture(2);
  const user3 = new UserFixture(3);
  const user4 = new UserFixture(8);
  const blockedUser = new UserFixture(4);
  const dialog = new DialogFixture(1);
  const dialog2 = new DialogFixture(2);

  beforeEach(async () => {
    await mongoose.connection.dropDatabase();
    await user1.save();
    await user2.save();
    await user4.save();
    await blockedUser.save();
    await blockedUser.block();
    client1.clear();
    client2.clear();
    client4.clear();
    clientBlocked.clear();
    await client1.authorize(user1);
    await client2.authorize(user2);
    await client4.authorize(user4);
    await clientBlocked.authorize(blockedUser);
    await dialog.save(user1, user2);
  });

  describe('create', () => {
    it('from active user to active user', async () => {
      client1.send({
        actionId: 'createDialog',
        path: ApiPath.DIALOG,
        method: RequestMethod.POST,
        body: {
          userId: user4.getId(),
        },
      });
      client1.waitAnswers(1);
      const answers = (await client1.getAnswers())[0];
      expect(answers).to.deep.include({
        actionId: 'createDialog',
        status: 201,
      });
      expect(answers.body.dialog).to.be.an('object');

      client4.waitRequests(1);
      const messageFomServer = (await client4.getRequests())[0];

      expect(answers.body.dialog).to.deep.include(messageFomServer.body.dialog);
    });

    it('from active user to blocked user', async () => {
      client1.send({
        actionId: 'createDialog',
        path: ApiPath.DIALOG,
        method: RequestMethod.POST,
        body: {
          userId: blockedUser.getId(),
        },
      });
      client1.waitAnswers(1);
      const answers = (await client1.getAnswers())[0];
      expect(answers).to.deep.include({
        actionId: 'createDialog',
        status: 403,
      });

      clientBlocked.waitRequests(1);
      const messageFomServer = (await clientBlocked.getRequests())[0];

      expect(messageFomServer).to.equal(undefined);
    });

    it('was created before', async () => {
      client1.send({
        actionId: 'createDialog',
        path: ApiPath.DIALOG,
        method: RequestMethod.POST,
        body: {
          userId: user2.getId(),
        },
      });
      client1.waitAnswers(1);
      const answers = (await client1.getAnswers())[0];
      expect(answers).to.deep.include({
        actionId: 'createDialog',
        status: 400,
      });
      expect(answers.body.errorMessage).to.be.an('string');

      client2.waitRequests(1);
      const messageFomServer = (await client2.getRequests())[0];

      expect(messageFomServer).to.equal(undefined);
    });

    it('was created before, another user is blocked in dialog', async () => {
      await dialog.block(user2);
      client1.send({
        actionId: 'createDialog',
        path: ApiPath.DIALOG,
        method: RequestMethod.POST,
        body: {
          userId: user2.getId(),
        },
      });
      client1.waitAnswers(1);
      const answers = (await client1.getAnswers())[0];
      expect(answers).to.deep.include({
        actionId: 'createDialog',
        status: 400,
      });
      expect(answers.body.errorMessage).to.be.an('string');

      client2.waitRequests(1);
      const messageFomServer = (await client2.getRequests())[0];

      expect(messageFomServer).to.equal(undefined);
    });
  });

  describe('update', () => {
    it('block user', async () => {
      client1.send({
        actionId: 'updateDialog',
        path: ApiPath.DIALOG,
        method: RequestMethod.PATCH,
        body: {
          dialogId: dialog.getId(),
          blockUserId: user2.getId(),
        },
      });
      client1.waitAnswers(1);
      let answers = (await client1.getAnswers())[0];
      expect(answers).to.deep.include({
        actionId: 'updateDialog',
        status: 200,
      });
      expect(answers.body.dialog.blocked.length).to.equal(1);
      expect(answers.body.dialog.blocked).to.includes(user2.getId());

      client2.waitRequests(1);
      let messageFomServer = (await client2.getRequests())[0];

      expect(answers.body.dialog).to.deep.include(messageFomServer.body.dialog);

      client2.send({
        actionId: 'updateDialog',
        path: ApiPath.DIALOG,
        method: RequestMethod.PATCH,
        body: {
          dialogId: dialog.getId(),
          blockUserId: user1.getId(),
        },
      });
      client2.waitAnswers(1);
      answers = (await client2.getAnswers())[0];
      expect(answers).to.deep.include({
        actionId: 'updateDialog',
        status: 200,
      });
      expect(answers.body.dialog.blocked.length).to.equal(2);
      expect(answers.body.dialog.blocked).to.includes(user1.getId());

      client1.waitRequests(1);
      messageFomServer = (await client1.getRequests())[0];

      expect(answers.body.dialog).to.deep.include(messageFomServer.body.dialog);
    });

    it('block blocked user', async () => {
      await dialog.block(user2);
      client1.send({
        actionId: 'updateDialog',
        path: ApiPath.DIALOG,
        method: RequestMethod.PATCH,
        body: {
          dialogId: dialog.getId(),
          blockUserId: user2.getId(),
        },
      });
      client1.waitAnswers(1);
      const answers = (await client1.getAnswers())[0];
      expect(answers).to.deep.include({
        actionId: 'updateDialog',
        status: 400,
      });

      expect(answers.body.errorMessage).to.be.an('string');

      client2.waitRequests(1);
      const messageFomServer = (await client2.getRequests())[0];

      expect(messageFomServer).to.equal(undefined);
    });

    it('unblock user', async () => {
      await dialog.block(user1);
      await dialog.block(user2);

      client1.send({
        actionId: 'updateDialog',
        path: ApiPath.DIALOG,
        method: RequestMethod.PATCH,
        body: {
          dialogId: dialog.getId(),
          unblockUserId: user2.getId(),
        },
      });
      client1.waitAnswers(1);
      let answers = (await client1.getAnswers())[0];
      expect(answers).to.deep.include({
        actionId: 'updateDialog',
        status: 200,
      });
      expect(answers.body.dialog.blocked.length).to.equal(1);
      expect(answers.body.dialog.blocked).to.not.includes(user2.getId());

      client2.waitRequests(1);
      let messageFomServer = (await client2.getRequests())[0];

      expect(answers.body.dialog).to.deep.include(messageFomServer.body.dialog);

      client2.send({
        actionId: 'updateDialog',
        path: ApiPath.DIALOG,
        method: RequestMethod.PATCH,
        body: {
          dialogId: dialog.getId(),
          unblockUserId: user1.getId(),
        },
      });
      client2.waitAnswers(1);
      answers = (await client2.getAnswers())[0];
      expect(answers).to.deep.include({
        actionId: 'updateDialog',
        status: 200,
      });
      expect(answers.body.dialog.blocked.length).to.equal(0);

      client1.waitRequests(1);
      messageFomServer = (await client1.getRequests())[0];

      expect(answers.body.dialog).to.deep.include(messageFomServer.body.dialog);
    });

    it('unblock not blocked user', async () => {
      client1.send({
        actionId: 'updateDialog',
        path: ApiPath.DIALOG,
        method: RequestMethod.PATCH,
        body: {
          dialogId: dialog.getId(),
          unblockUserId: user2.getId(),
        },
      });
      client1.waitAnswers(1);
      const answers = (await client1.getAnswers())[0];
      expect(answers).to.deep.include({
        actionId: 'updateDialog',
        status: 400,
      });

      expect(answers.body.errorMessage).to.be.an('string');

      client2.waitRequests(1);
      const messageFomServer = (await client2.getRequests())[0];

      expect(messageFomServer).to.equal(undefined);
    });

    it('non-existent dialogs user', async () => {
      client1.send({
        actionId: 'updateDialog',
        path: ApiPath.DIALOG,
        method: RequestMethod.PATCH,
        body: {
          dialogId: dialog.getId(),
          blockUserId: user3.getId(),
        },
      });
      client1.waitAnswers(1);
      let answers = (await client1.getAnswers())[0];
      expect(answers).to.deep.include({
        actionId: 'updateDialog',
        status: 403,
      });

      expect(answers.body.errorMessage).to.be.an('string');
      client1.send({
        actionId: 'updateDialog',
        path: ApiPath.DIALOG,
        method: RequestMethod.PATCH,
        body: {
          dialogId: dialog.getId(),
          unblockUserId: user3.getId(),
        },
      });
      client1.waitAnswers(1);
      answers = (await client1.getAnswers())[0];
      expect(answers).to.deep.include({
        actionId: 'updateDialog',
        status: 403,
      });

      expect(answers.body.errorMessage).to.be.an('string');
    });
  });

  describe('delete', () => {
    it('correct first user requester', async () => {
      const messageObj = await dialog.createMessage(user1, 'привет');
      client1.send({
        actionId: 'deleteDialog',
        path: ApiPath.DIALOG,
        method: RequestMethod.DELETE,
        body: {
          dialogId: dialog.getId(),
        },
      });
      client1.waitAnswers(1);
      const answers = (await client1.getAnswers())[0];
      expect(answers).to.deep.include({
        actionId: 'deleteDialog',
        status: 200,
      });

      const message = await messageService.getById(messageObj._id);
      expect(message).to.equal(null);

      client2.waitRequests(1);
      const messageFomServer = (await client2.getRequests())[0];

      expect(dialog.getId()).to.deep.include(messageFomServer.body.dialogId);
    });

    it('correct second user requester', async () => {
      client2.send({
        actionId: 'deleteDialog',
        path: ApiPath.DIALOG,
        method: RequestMethod.DELETE,
        body: {
          dialogId: dialog.getId(),
        },
      });
      client2.waitAnswers(1);
      const answers = (await client2.getAnswers())[0];
      expect(answers).to.deep.include({
        actionId: 'deleteDialog',
        status: 200,
      });

      client1.waitRequests(1);
      const messageFomServer = (await client1.getRequests())[0];

      expect(dialog.getId()).to.deep.include(messageFomServer.body.dialogId);
    });

    it('non-exist', async () => {
      client1.send({
        actionId: 'deleteDialog',
        path: ApiPath.DIALOG,
        method: RequestMethod.DELETE,
        body: {
          dialogId: dialog2.getId(),
        },
      });
      client1.waitAnswers(1);

      const answers = (await client1.getAnswers())[0];
      expect(answers).to.deep.include({
        actionId: 'deleteDialog',
        status: 404,
      });

      client2.waitRequests(1);
      const messageFomServer = (await client2.getRequests())[0];

      expect(messageFomServer).to.equal(undefined);
    });
  });
});
